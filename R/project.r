##################################################################
# Run once to load project
##################################################################

# singlton stuff
source("R/single.r")
reset.source()
ca.clean()
register.source("R/single.r")

# source files
register.source("R/coverage.r")
register.source("R/colored_matrix.r")
register.source("R/cmap.r")
register.source("R/scell_qc.r")
