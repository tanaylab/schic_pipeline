##################################
# Plot qc figures in a single plot
##################################
plot.qc.figures=function(datasets = "cell1", odir="results", fdir= "figures/qc/", obs.dist.binsize=10^6, 
  normalize.marginals = F, raw = F, cmap.binsize = 10*10^6, title = "p", width = 6, chroms=c(1:19, 'X'), chrs.xlimit = NULL, chrs.ylimit = NULL, type = "o", cmap.breaks = c(0, 1, 4, 50, 200, 600), cmap.colors = c("white", "yellow", "orange", "red", "purple", "black"), fig.width=12, fig.height=9, max.amp=10, haploid=F)
{
  source("./R/project.r")
  system(paste("mkdir -p", fdir))

  for (i in 1:length(datasets)) {
    dataset = datasets[i]

    ofn = paste(fdir, "/", dataset, "_qc_pack.eps", sep="")

    start.plot(ofn, device="postscript", width=fig.width, height=fig.height)
    layout(matrix(c(6,4,5,1,2,3,8,7,7,9,7,7),3,4), widths=c(1,1,1,1), heights=c(1,1,1))
    fdir = ""

    cat("amplification ... ")
    amplification(dataset=dataset, odir=odir, fdir=fdir, main.str="d")

    cat("observed.distrib ... ")
    observed.distrib(dataset=dataset, odir=odir, fdir=fdir, binsize=obs.dist.binsize, main.str="e")

    cat("fend.coverage.obs.vs.exp ... ")
    fend.coverage.obs.vs.exp(dataset=dataset, odir=odir, fdir=fdir, haploid=haploid[i], main.str="f")

    cat("cis.ratio.by.amp ...")
    cis.ratio.by.amp(dataset=dataset, odir=odir, fdir=fdir, max.amp=max.amp, main.str="b")

    cat("cis.ratio.by.mul ...")
    cis.ratio.by.mul(dataset=dataset, odir=odir, fdir=fdir, main.str="c")

    cat("amplification.mul.vs.single ...")
    amplification.mul.vs.single(dataset=dataset, odir=odir, fdir=fdir, hits.cutoff=3, main.str="a")

    cat("spatial.plot ...")
    spatial.plot(ifn.prefix=paste(odir, "/", dataset, sep=""), fdir=fdir, dataset=dataset, type=type,
                 ofn.prefix=title, colors=cmap.colors, breaks=cmap.breaks, binsize=cmap.binsize,
                 chroms=chroms, chrs.xlimit=chrs.xlimit, chrs.ylimit=chrs.ylimit,
                 raw=raw, normalize.marginals=normalize.marginals, width=width, plot.cutoff=F, plot.legend=F, main.str="i")

    cat("fends.total ...")
    fends.total(dataset=dataset, odir=odir, fdir=fdir, main.str="g")

    cat("fends.cis.exp ...")
    fends.cis.exp(dataset=dataset, odir=odir, fdir=fdir, main.str="h")

    par(oma=c(0,0,2,0))
    title(main=dataset, outer=T, cex=2)
    
    end.plot(ofn)
  }

}
