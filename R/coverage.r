###################################################################################
# fend and contact coverage distributions
####################################################################################

###############################
# Fend coverage (hits per fend)
fend.coverage=function(dataset="E1a", odir="results", fdir="figures/fend_cov/", only.x=F, only.auto=F, gen.plot=T, max.hits=5, only.trans=F, n.bars=20)
{
  ca.context(context="fend.coverage")
  mat = ca.get.table(paste(odir, "/", dataset, ".mat", sep=""))
  fends = sort(c(mat$fend1, mat$fend2))
  title = "all"
  fends.map= ca.get.table(paste(odir, "/", dataset, ".fends", sep=""))
  n.fends = nrow(fends.map)
  if (only.x || only.auto || only.trans) {

    m = ca.get(paste(odir, dataset, "merged_mat"), merge.mat.to.fends(mat, fends.map))
    
    if (only.x) {
      mat.x1 = m[m$chr1 == 'X','fend1']
      mat.x2 = m[m$chr2 == 'X','fend2']
      fends = sort(c(mat.x1, mat.x2))
      title = "X"
      n.fends = nrow(fends.map[fends.map$chr == 'X',])
    }
    else if (only.auto) {
      mat.auto1 = m[m$chr1 != 'X','fend1']
      mat.auto2 = m[m$chr2 != 'X','fend2']
      fends = sort(c(mat.auto1, mat.auto2))
      title = "auto"
      n.fends = nrow(fends.map[fends.map$chr != 'X',])
    }
    else if (only.trans) {
      mat.auto1 = m[m$chr1 != m$chr2,'fend1']
      mat.auto2 = m[m$chr2 != m$chr2,'fend2']
      fends = sort(c(mat.auto1, mat.auto2))
      title = "trans"
    }
  }

  count = length(unique(fends))
        
  rle = rle(fends)
  coverage = sort(rle$len, dec=T)
  coverage = coverage[coverage <= max.hits]
  
  r=rle(sort(coverage))

  system(paste("mkdir -p ", odir, "/qc/hpf", sep=""))
  write.table(data.frame(hits=c(0, r$val),count=c(n.fends - count, r$len)),
              paste(odir, "/qc/hpf/hpf_", title, "_", dataset, ".txt", sep=""), quote=F, sep="\t", row.names=F)

  if (gen.plot) {
    if (fdir != "") {
      system(paste("mkdir -p", fdir))
      ofn = paste(fdir,  "cov_", title, "_", dataset, ".png", sep="")
      start.plot(ofn, device="png", width=300, height=300)
    }
    
    if (length(fends) > 0) {
      hist(coverage, n.bars, main=paste(dataset, ": ", title, ", N=", length(coverage), sep=""), xlab=paste("coverage (>=3: ", sum(coverage>2), ")", sep=""),
           col="orange", xlim=c(1,5), cex=1)
    }
    else{
      plot(0, type="l", xlab="", ylab="", main="No fends on chr X", cex=2,xlim=c(0,0), ylim=c(0,0))
    }
    
    if (fdir != "") {
      end.plot(ofn)
    }
  }

}


##############################
# Extract multiple hits fends
##############################
mul.hits.fends=function(dataset="marD_5", odir="results", fdir="figures/hpf_hits/", min.hits=3)
{
  ca.context(context="fend.coverage")
  mat = ca.get.table(paste(odir, "/", dataset, ".mat", sep=""))
  fends = sort(c(mat$fend1, mat$fend2))
  fends.map= ca.get.table(paste(odir, "/", dataset, ".fends", sep=""))

  m = ca.get(paste(odir, dataset, "merged_mat"), merge.mat.to.fends(mat, fends.map))

  rle = rle(fends)

  mul.ids = data.frame(fend=rle$values[rle$lengths >= min.hits], hits=rle$lengths[rle$lengths >= min.hits])
  res = rbind(merge(m, mul.ids, by.x="fend1", by.y="fend"), merge(m, mul.ids, by.x="fend2", by.y="fend"))

  if (fdir != "") {
    system(paste("mkdir -p ", odir, "/qc/mul_fends", sep=""))
    write.table(res, paste(fdir, "/qc/mul_fends/min", min.hits, "_hits_", dataset, ".txt", sep=""), quote=F, sep="\t", row.names=F)
  }
    

  mul.fends = merge(fends.map[,c("fend", "strand", "chr", "coord")], mul.ids, by.x="fend", by.y="fend")
  list(fends=mul.fends[,c("fend","chr", "strand", "coord")], data=res)
}

##########################################################
# cis ratio in fends pairs breakdown by fend multiplicity
##########################################################
cis.ratio.by.mul=function(dataset="marD_5", odir="results", fdir="figures/hpf_p_cis/", main.str="")
{
  ca.context(context="fend.coverage")
  mat = ca.get.table(paste(odir, "/", dataset, ".mat", sep=""))
  fends = sort(c(mat$fend1, mat$fend2))
  fends.map= ca.get.table(paste(odir, "/", dataset, ".fends", sep=""))

  m = ca.get(paste(odir, dataset, "merged_mat"), merge.mat.to.fends(mat, fends.map))

  rle = rle(fends)

  r.x = c()
  r.x.ci = c()
  r.auto = c()
  r.auto.ci = c()
  n.pairs = c()

  max.hits = min(15, max(0, max(rle$lengths)))

  for (i in 1:max.hits) {
    ids = merge(fends.map[,c("fend", "chr")], data.frame(fend=rle$values[rle$lengths == i]))

    x.ids = ids[ids$chr == 'X',]
    x.res = rbind(merge(m, x.ids, by.x="fend1", by.y="fend"), merge(m, x.ids, by.x="fend2", by.y="fend"))
    r.x[i] = nrow(x.res[x.res$chr1 == x.res$chr2,]) / nrow(x.res)
    r.x.ci[i] = agresti.coull.ci(x=nrow(x.res[x.res$chr1 == x.res$chr2,]), n=nrow(x.res), alpha=0.05)
    
    auto.ids = ids[ids$chr != 'X',]
    auto.res = rbind(merge(m, auto.ids, by.x="fend1", by.y="fend"), merge(m, auto.ids, by.x="fend2", by.y="fend"))
    r.auto[i] = nrow(auto.res[auto.res$chr1 == auto.res$chr2,]) / nrow(auto.res)
    r.auto.ci[i] = agresti.coull.ci(x=nrow(auto.res[auto.res$chr1 == auto.res$chr2,]), n=nrow(auto.res), alpha=0.05)
    
    n.pairs[i] = nrow(x.res) + nrow(auto.res)
  }

  res = data.frame(hits=1:max.hits, p.cis.x=r.x, cis.x.ci=r.x.ci, p.cis.auto=r.auto, cis.auto.ci=r.auto.ci)

  system(paste("mkdir -p ", odir, "/qc/cis_ratio", sep=""))
  write.table(res, paste(odir, "/qc/cis_ratio/cis_ratio_by_hits_", dataset, ".txt", sep=""), quote=F, sep="\t", row.names=F)

  #res = res[n.pairs > 16,]
  
  if (fdir != "") {
    system(paste("mkdir -p", fdir))
    ofn = paste(fdir,  "p_cis_by_hit_", dataset, ".png", sep="")
    start.plot(ofn, device="png", width=250, height=250)
  }

  
  plot.new()

  #xlim=c(1,dim(res)[1]-1)
  xlim=c(1,min(dim(res)[1], max.hits))
  plot.window(xlim=xlim, ylim=c(0,1))
  par("cex.axis"=1)
  box()
  title(main=main.str, xlab="hits per fend", ylab="cis fraction")
  x.ticks = xlim[1]:xlim[2]
  axis(1, at=x.ticks, lab=x.ticks)
  axis(2,las=1)
  grid()

  points(res$hits, res$p.cis.auto, col="black")
  suppressWarnings(plotCI(x=res$hits, y=res$p.cis.auto, uiw=res$cis.auto.ci, type='l', col="dark green", lwd=2, add=T))

#  lines(res$hits, res$p.cis.x, col="brown", lwd=2)  
  points(res$hits, res$p.cis.x, col="black")
  suppressWarnings(plotCI(x=res$hits, y=res$p.cis.x, uiw=res$cis.x.ci, type='l', col="brown", lwd=2, add=T))
  
  legend(x='bottomleft', legend=c("auto", "X"), lty=c(1,1), col=c("dark green", "brown"), cex=0.8, bty='n')

  if (fdir != "") {
    end.plot(ofn)
  }
}

###########################################################
# cis ratio in fends pairs breakdown by reads multiplicity
###########################################################
cis.ratio.by.amp=function(dataset="TN_SD_2", odir="results", fdir="figures/cis_ratio_by_amp/", max.amp=10, main.str="")
{
  ca.context(context="fend.coverage")
  mat = ca.get.table(paste(odir, "/", dataset, ".mat", sep=""))
  fends.map= ca.get.table(paste(odir, "/", dataset, ".fends", sep=""))

  m = ca.get(paste(odir, dataset, "merged_mat"), merge.mat.to.fends(mat, fends.map))

  m[m$count >= max.amp,'count'] = max.amp
  max.amp = max(m$count)
  
  s.m = split(m, m$count)
  
  r = rep(0, max.amp)
  r.ci = rep(0, max.amp)

  for (i in 1:max.amp) {
   
    c.m = s.m[[as.character(i)]]
    if (!is.null(c.m)) {
      n.cis = nrow(c.m[c.m$chr1 == c.m$chr2,])
      n.tot = nrow(c.m)
      r[i] =  n.cis / n.tot
      r.ci[i] = agresti.coull.ci(x=n.cis, n=n.tot, alpha=0.05)
    }
  }

  if (fdir != "")
    system(paste("mkdir -p", fdir))
  res = data.frame(amp=1:max.amp, p.cis=r, cis.ci=r.ci)

  system(paste("mkdir -p ", odir, "/qc/cis_ratio/", sep=""))
  write.table(res, paste(odir, "/qc/cis_ratio/cis_ratio_by_amp_", dataset, ".txt", sep=""), quote=F, sep="\t", row.names=F)

  if (fdir != "") {
    ofn = paste(fdir,  "p_cis_by_amp_", dataset, ".png", sep="")
    start.plot(ofn, device="png", width=250, height=250)
  }
  
  plot.new()

  xlim=c(1, max.amp)

  r.ylim = r[r > 0]
  r.ylim.ci = r.ci[r > 0]
  ylim = c(min(r.ylim[1:5] - r.ylim.ci[1:5]), max(r.ylim[1:5] + r.ylim.ci[1:5]))
  plot.window(xlim=xlim, ylim=ylim)
  par("cex.axis"=1)
  box()
  title(main=main.str, xlab="amplification factor", ylab="cis fraction")
  x.ticks = xlim[1]:xlim[2]
  axis(1, at=x.ticks, lab=x.ticks)
  axis(2,las=1)
  grid()

  points(res$amp, res$p.cis, col="black")
  suppressWarnings(plotCI(x=res$amp, y=res$p.cis, uiw=res$cis.ci, type='l', col="dark green", lwd=2, add=T))  

  if (fdir != "") {
    end.plot(ofn)
  }
}
#############################################
# Calculate Agrest-Coull confidence intervals
#############################################
agresti.coull.ci=function(x, n, alpha=0.05)
{
  z = qnorm(p=(1-alpha/2))
  nhat = n + z^2
  phat = (x + z^2/2) / nhat

  z * sqrt( (phat * (1 - phat) ) / nhat)
  
}

######################################################################
# fend coverage distrib: observed vs expected by binomial distribution
######################################################################
fend.coverage.obs.vs.exp=function(dataset="marD_5", odir="results", fdir="figures/hpf_hits/", haploid=F, global.xlim=c(NA,NA), main.str="")
{
  if (fdir != "")
    system(paste("mkdir -p", fdir))
  
  ca.context(context="fend.coverage")
  fend.coverage(dataset=dataset, odir=odir, fdir=fdir, only.x=T, gen.plot=F, max.hits=12)
  x = read.delim(paste(odir, "/qc/hpf/hpf_X_", dataset, ".txt", sep=""))
  
  if (haploid) {
    fend.coverage(dataset=dataset, odir=odir, fdir=fdir, gen.plot=F, max.hits=12)
    auto = read.delim(paste(odir, "/qc/hpf/hpf_all_", dataset, ".txt", sep=""))
    count = sum(auto[auto$hits > 0, 'count'])
  }
  else {
    fend.coverage(dataset=dataset, odir=odir, fdir=fdir, only.auto=T, gen.plot=F, max.hits=12)
    auto = read.delim(paste(odir, "/qc/hpf/hpf_auto_", dataset, ".txt", sep=""))
    count = sum(c(x[x$hits > 0, 'count'], auto[auto$hits > 0, 'count']))
  }
  
  # filter out <= 1 occurances
  #auto = auto[auto$count > 1,]
  #x = x[x$count > 1,]

  n.auto.fends = sum(auto$count)

  total.auto.obs = crossprod(auto$hits, auto$count)
  
  auto.obs = log2(auto$count)
  pa = auto[auto$hits == 1,'count'] / n.auto.fends

  # Binomial
  auto.pred = log2(n.auto.fends * dbinom(prob=1/n.auto.fends, size=total.auto.obs, x=auto$hits))
  # Poisson auto.pred = log2(dpois(x=auto$hits, lambda=pa) * n.auto.fends)

  # Estimate probability for the observed number of auto fends with 3 hits under the binomial assumption and 2 cells simulation
  n.auto.hits = if (haploid) 2 else 3
  n.chrs.for.sim = if (haploid) 2 else 4
  p.vals.auto = compute.mul.hits.p.val(n.fends=n.auto.fends, n.reads=total.auto.obs, n.hits=n.auto.hits, counts=auto, n.chrs.for.sim=n.chrs.for.sim)
  

  if (haploid) {
    x.obs = NULL
    total.x.obs = 0
    x.pred = rep(0, length(auto.pred))
    p.vals.x = NULL
  }
  else {
    n.x.fends = sum(x$count)
    total.x.obs = crossprod(x$hits, x$count)
    x.obs = log2(x$count)
    px = x[x$hits == 1,'count'] / n.x.fends
    
    if (total.x.obs > 0) {
      # Binomial
      x.pred = log2(n.x.fends * dbinom(prob=1/n.x.fends, size=total.x.obs, x=auto$hits))
      # Poisson: x.pred = log2(dpois(x=auto$hits, lambda=px) * n.x.fends)
    }
    else {
      x.pred = rep(0, length(auto.pred))
    }

    # Estimate probability for the observed number of X fends with 2 hits under the binomial assumption and 2 cells simulation
  
    p.vals.x = compute.mul.hits.p.val(n.fends=n.x.fends, n.reads=total.x.obs, n.hits=2, counts=x, n.chrs.for.sim=2)
  }
  
  if (fdir != "") {
    ofn = paste(fdir,  "hpf_obs_", dataset, ".eps", sep="")
    start.plot(ofn, width=3, height=3)
  }
  

  if (total.auto.obs > 0) {
    system(paste("mkdir -p ", odir, "/qc/hpf", sep=""))
    write.table(data.frame(hits=auto$hits, auto.log.count=auto.obs, x.log.count=c(x.obs, rep(0, length(auto.obs) - length(x.obs))), auto.pred=auto.pred, x.pred=x.pred), paste(odir, "/qc/hpf/hpf_obs_vs_exp_", dataset, ".txt", sep=""), quote=F, sep="\t", row.names=F)

    write.table(rbind(p.vals.auto, p.vals.x), paste(odir, "/qc/hpf/hpf_obs_p_vals_", dataset, ".txt", sep=""), quote=F, sep="\t", row.names=F)
  }
  
  xlim = if (is.na(global.xlim[1])) range(c(auto$hits, x$hits)) else global.xlim
  ylim = c(1, max(x.obs, auto.obs))

  plot.new()
  plot.window(xlim=xlim, ylim=ylim)
  par("cex.axis"=1)
  box()
  #main.str = paste(dataset, ' N=', count)
  title(main=main.str, xlab="hits per fend", ylab="log2(count)")
  x.ticks = xlim[1]:xlim[2]
  axis(1, at=x.ticks, lab=x.ticks)
  axis(2,las=1)
  grid()


  leg.titles = if (haploid) c('obs', 'pred') else c('auto', 'chrX', 'pred')
  leg.cols = if (haploid) c('dark green', 'black') else c('dark green', 'brown', 'black')
  leg.lty = if (haploid) c(1,2) else c(1,1,2)
  leg.lwd = if (haploid) c(2,1) else c(2,2,1)
  leg.ind = if (haploid) c(FALSE, TRUE) else c(FALSE, FALSE, TRUE)
  
  #expected distribution vs. observed hits per fend
  if (total.auto.obs > 0) {
    lines(auto$hits, auto.obs, col="dark green", lwd=2)  
    points(auto$hits, auto.obs, col="black")

    # binoimal
    lines(auto$hits, auto.pred, type="l", lty=2)
    
    leg.ind[1] = TRUE
  }

  
  if (!haploid & total.x.obs > 0) {
    lines(x$hits, x.obs, col="brown", lwd=2)  
    points(x$hits, x.obs, col="black")

    # Binomial prediction
    lines(auto$hits, x.pred, type="l", lty=2)
    leg.ind[2] = TRUE
  }
  
  legend(x='bottomleft', legend=leg.titles[leg.ind], col=leg.cols[leg.ind], lty=leg.lty[leg.ind], lwd=leg.lwd[leg.ind], cex=0.7, bty='n')
  
  if (fdir != "") {
    end.plot(ofn)
  }
}

###########################
compute.mul.hits.p.val=function(n.fends=n.auto.fends, n.reads=total.auto.obs, n.hits=3, counts=auto, n.chrs.for.sim=4)
{
  # P val under the binomial model (unlimited number of cells)
  p.hits = dbinom(prob=1/n.fends, size=n.reads, x=n.hits)
  obs.hits = if (is.element(n.hits, counts$hits)) counts[counts$hits == n.hits, 'count'] else 0
  pred.hits = p.hits * n.fends
  p.val.binom = pnorm( (obs.hits - pred.hits) / sqrt(n.fends * p.hits * (1 - p.hits)))

  # P val under the given number of chromosomes
  sim.res = fend.coverage.sim(n.fends=n.fends, n.reads=n.reads, n.chrs=n.chrs.for.sim, n.reps=1000, max.k=n.hits)
  sim.few.chrs.m = mean(sim.res[n.hits + 1, ])
  sim.few.chrs.sd = sd(sim.res[n.hits + 1, ])
  p.val.few.chrs = pnorm( (obs.hits - sim.few.chrs.m) / sqrt(sim.few.chrs.sd))

  list(hits=n.hits, chrs=n.chrs.for.sim, obs.hits=obs.hits, pred.binom.hits=pred.hits, p.val.binom=p.val.binom, sim.few.cells.hits=sim.few.chrs.m, p.val.few.chrs=p.val.few.chrs)
}

#######################################################
# Emperically simulate multiply hit fends by simulation
fend.coverage.sim=function(n.fends=1*10^6, n.reads=25000, n.chrs=4, n.reps=1000, max.k=4)
{

  result = c()
  for (i in 1:n.reps) {
    #cat(sprintf("rep %g\n", i))
    r = rle(sort(sample.int(n.fends*n.chrs, n.reads) %% n.fends))$lengths
    counts = sapply(1:max.k, function(x){sum(r==x)})

    result = c(result, c(n.fends - sum(counts), counts))
  }

  matrix(result, ncol=n.reps, nrow=(max.k+1))
}

######################################################
# breakdown of read-pair amplification number by type
######################################################
amplification=function(dataset="E1a", odir="results", fdir="figures/amplification/", main.str="")
{
  if (fdir != "")
    system(paste("mkdir -p", fdir))
  
  ca.context(context="fend.coverage")
  mat = ca.get.table(paste(odir, "/", dataset, ".mat", sep=""))
  fends = ca.get.table(paste(odir, "/", dataset, ".fends", sep=""))
  mat = ca.get(paste(odir, dataset, "merged_mat"), merge.mat.to.fends(mat, fends))
  
  mat.trans = mat[mat$chr1 != mat$chr2,]
  mat.close.cis = mat[mat$chr1 == mat$chr2 & abs(mat$coord1-mat$coord2)<1000000,]
  mat.far.cis = mat[mat$chr1 == mat$chr2 & abs(mat$coord1-mat$coord2)>1000000,]
  
  if (nrow(mat.trans) > 0) {
    ecdf.trans = ecdf(mat.trans$count)
  }
  if (nrow(mat.close.cis) > 0) {
    ecdf.close.cis = ecdf(mat.close.cis$count)
  }
  if (nrow(mat.far.cis) > 0) {
    ecdf.far.cis = ecdf(mat.far.cis$count)
  }

  if (fdir != "") {
    ofn = paste(fdir,  "amp_", dataset, ".png", sep="")
    start.plot(ofn, device="png", width=300, height=400)
  }

  par("cex"=1)

  if (nrow(mat.close.cis) > 0 && nrow(mat.far.cis) > 0 && (nrow(mat.trans) > 0)) {
    xlim = c(1, max(mat.trans$count, mat.close.cis$count, mat.far.cis$count))

    plot(ecdf.close.cis, verticals=T, do.points=F, main=main.str,
         xlab="amplification factor", ylab="ecdf", log="x", xlim=xlim, col.vert="red", col.hor="red")
    plot(ecdf.far.cis, verticals=T, do.points=F, add=T, col.vert="blue", col.hor="blue")
    plot(ecdf.trans, verticals=T, do.points=F, add=T, col.vert="black", col.hor="black")
    legend("bottomright", legend=c("trans", ">1M", "<1M"), fill=c("black", "blue", "red"))
  }
  
  if (fdir != "")
    end.plot(ofn)
}


####################################################################################
# Breakdown of read-pair amplification factor by number of times a fend was covered
####################################################################################
amplification.mul.vs.single=function(dataset="E1a", odir="results", fdir="figures/mul_cov_amplification/", hits.cutoff=3, main.str="")
{
  if (fdir != "")
    system(paste("mkdir -p", fdir))
  
  ca.context(context="fend.coverage")
  mat = ca.get.table(paste(odir, "/", dataset, ".mat", sep=""))

  mul.data = mul.hits.fends(dataset=dataset, odir=odir, min.hits=hits.cutoff, fdir=fdir)
  
  m1 = merge(mat, mul.data$fends, by.x="fend1", by.y="fend", all.x=T)
  m2 = merge(mat, mul.data$fends, by.x="fend2", by.y="fend", all.x=T)

  mul.amp.counts = c(m1[!is.na(m1$coord), "count"], m2[!is.na(m2$coord), "count"])
  non.mul.amp.counts = c(m1[is.na(m1$coord), "count"], m2[is.na(m2$coord), "count"])

  if (fdir != "") {
    ofn = paste(fdir,  "amp_", dataset, ".png", sep="")
    start.plot(ofn, device="png", width=300, height=400)
  }
  
  to.plot = F
  xlim = c(1, max(c(mul.amp.counts, non.mul.amp.counts)))
  par("cex"=1)

  if (length(non.mul.amp.counts) > 0) {
    ecdf.non.mul = ecdf(non.mul.amp.counts)
    plot(ecdf.non.mul, verticals=T, do.points=F, main=main.str,
         xlab="amplification factor", ylab="ecdf", log="x", xlim=xlim, col.vert="red", col.hor="red")
    to.plot = T
  }
  
  if (length(mul.amp.counts) > 0) {
    ecdf.mul = ecdf(mul.amp.counts)
    plot(ecdf.mul, verticals=T, do.points=F, add=T, col.vert="blue", col.hor="blue")
    to.plot = T
  }

  legend("bottomright", legend=c(sprintf("<%g hits", hits.cutoff), sprintf(">=%g hits", hits.cutoff)), fill=c("red", "blue"), bty='n', cex=0.9)
  
  if (fdir != "")
    end.plot(ofn)
}

##########################################
# fends pairs (cis vs trans) per dataset
##########################################
fends.total=function(dataset="E1a", odir="results", fdir="figures/breakdown/", main.str="")
{
  if (fdir != "")
    system(paste("mkdir -p", fdir))
  
  ca.context(context="fend.coverage")
  mat = ca.get.table(paste(odir, "/", dataset, ".mat", sep=""))
  fends = ca.get.table(paste(odir, "/", dataset, ".fends", sep=""))
  mat = ca.get(paste(odir, dataset, "merged_mat"), merge.mat.to.fends(mat, fends))

  mat.trans = mat[mat$chr1 != mat$chr2,]
  mat.close.cis = mat[mat$chr1 == mat$chr2 & abs(mat$coord1-mat$coord2)<1000000,]
  mat.far.cis = mat[mat$chr1 == mat$chr2 & abs(mat$coord1-mat$coord2)>1000000,]
  
  if (fdir != "") {
    ofn = paste(fdir,  "total_", dataset, ".png", sep="")
    start.plot(ofn, device="png", width=250, height=250)
  }

  counts = c(nrow(mat.close.cis), nrow(mat.far.cis), nrow(mat.trans)) / 1000
  names(counts) = c("<1M", ">1M", "Trans")
  par("cex.axis"=1)
  barplot(counts, horiz=F, col=c("red","blue","black"), main=sprintf("%s: reads", main.str), ylab="count (k)")
  
  if (fdir != "")
    end.plot(ofn)
}

####################################################################
# Expected cis fends pairs normalized by expected trans per dataset
####################################################################
fends.cis.exp=function(dataset="E1a", odir="results", fdir="figures/breakdown/", main.str="")
{
  if (fdir != "")
    system(paste("mkdir -p", fdir))
  
  ca.context(context="fend.coverage")
  mat = ca.get.table(paste(odir, "/", dataset, ".mat", sep=""))
  fends = ca.get.table(paste(odir, "/", dataset, ".fends", sep=""))
  
  mat = ca.get(paste(odir, dataset, "merged_mat"), merge.mat.to.fends(mat, fends))

  mat.trans = as.double(nrow(mat[mat$chr1 != mat$chr2,]))
  mat.cis = as.double(nrow(mat[mat$chr1 == mat$chr2,]))

  total.fends = as.double(nrow(fends) ** 2)
  cis.fends = split(fends, fends$chr)
  exp.cis = 0

  for (i in 1:length(cis.fends))
  {
     exp.cis = exp.cis + nrow(cis.fends[[i]]) ** 2
  }
  exp.trans = (mat.cis + mat.trans) * (1 - exp.cis / total.fends)
  exp.cis = (mat.cis + mat.trans) * (exp.cis / total.fends)
  
  enr.cis = log2(mat.cis) - log2(exp.cis)
  enr.trans = log2(mat.trans) - log2(exp.trans)
  
  if (fdir != "") {
    ofn = paste(fdir,  "ratio_", dataset, ".png", sep="")
    start.plot(ofn, device="png", width=250, height=250)
  }

  counts = c(enr.cis, enr.trans)
  
  names(counts) = c("cis", "trans")
  
  par("cex.axis"= 1.2)
  barplot(counts, horiz=F, col=c("red","black"), main=sprintf("%s: cis - trans: %.2f", main.str, enr.cis - enr.trans), ylab="log enrichment", ylim=c(-5,5))

  system(paste("mkdir -p ", odir, "/qc/cis_ratio/", sep=""))
  write(enr.cis - enr.trans, paste(odir, "/qc/cis_ratio/", dataset, "_cis_vs_trans_log_enrich.txt", sep=""))
  
  if (fdir != "")
    end.plot(ofn)

}

##################################################################################################
# Observed coverage of trans-chromosomal bins compared to a binomial distribution expected counts
##################################################################################################
observed.distrib=function(dataset="E1a", odir="results", fdir="figures/trans_hits/", binsize=10*10^6, min.hits=10, max.x=NULL, max.y=NULL, main.str="")
{
  if (fdir != "")
    system(paste("mkdir -p", fdir))

  ca.context(context="fend.coverage")
  bin.str = format(binsize, sci=F)
  
  ifn = paste(odir, "/", dataset, "_", bin.str, ".o_contact", sep="")

  contact = ca.get.table(ifn)
  contact = contact[contact$chr1 != contact$chr2,]

  cbins = ca.get.table(paste(odir, "/", dataset, "_", bin.str, ".cbins", sep=""))
  count = contact$unique_observed_count

  zero.count = sum(sapply(split(cbins, cbins$chr), function(x) { nrow(x) * (nrow(cbins) - nrow(x)) })) / 2 - nrow(contact)
 
  count = c(count, rep(0, zero.count))
  if (fdir != "") {
    ofn = paste(fdir,  "distrib_", dataset, "_", bin.str, ".png", sep="")
    start.plot(ofn, device="png", width=250, height=250)
  }

  hist = hist(count, breaks=-1:max(count)+0.5, plot=F)
  x = hist$mids
  y = hist$counts

  # keep only with more than min.hits count
                                        #  print(cbind(x,y))
  ind = y >= min.hits
  x = x[ind]
  y = y[ind]
  
  
  # log scale
  y = log2(y)
  
  #main = paste(dataset, " ", n2str(binsize), " N=", sum(count), sep="")
  xlim = if (is.null(max.x)) range(x) else c(0,max.x)
  ylim = if (is.null(max.y)) c(0,range(y)[2]) else c(0,max.y)

  plot.new()
  plot.window(xlim=xlim, ylim=ylim)
  par("cex.axis"=1)
  box()
  title(main=main.str, xlab="contacts per bin", ylab="log2(count)")
  axis(1)
  axis(2,las=1)
  grid()

  lines(x, y, col="red")  
  points(x, y, col="black", pch=19)

  if (fdir != "")
    write.table(data.frame(hits=x,log.count=y), paste(fdir, "hits_", dataset, "_", bin.str, ".txt", sep=""), quote=F, sep="\t", row.names=F)

  #total reads
  reads=sum(2**y * x)
  #total pairs
  pairs=sum(2**y)
                                        
  #binomial distribution vs. observed couts : observed are much more coupled than expected:
  
  lines(x,log2(pairs*dbinom(prob=1/pairs, size=reads, x=x)), type="l", lty=2)

  if (fdir != "") {
    end.plot(ofn)
  }
}

merge.mat.to.fends=function(mat, fends.map)
{
  m = merge(mat, fends.map[,c("fend", "strand", "chr", "coord")], by.x="fend1", by.y="fend")
  names(m)[c((dim(m)[2]-2):dim(m)[2])] = c("strand1", "chr1", "coord1")
  m = merge(m, fends.map[,c("fend", "strand", "chr", "coord")], by.x="fend2", by.y="fend")
  names(m)[c((dim(m)[2]-2):dim(m)[2])] = c("strand2", "chr2", "coord2")
  m  
}
  
