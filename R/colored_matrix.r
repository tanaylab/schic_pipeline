#########################################################################################################
# Plot matrix : utility function
#########################################################################################################

smatrix2matrix=function(smatrix, dim, i.field="i", j.field="j", value.field="value", default.value=0)
{
  indices = smatrix[,i.field] + (smatrix[,j.field]-1) * dim[1]
  v = rep(default.value, dim[1]*dim[2])
  v[indices] = smatrix[,value.field]
  matrix(v, dim[1], dim[2])
}

matrix2smatrix=function(matrix)
{
  dim1 = dim(matrix)[1]
  dim2 = dim(matrix)[2]
  v = as.vector(matrix)
  indices = 1:(dim1*dim2)
  i = (indices-1) %% dim1 + 1
  j = floor((indices-1) / dim1) + 1
  data.frame(i=i, j=j, value=v, stringsAsFactors=F)
}

make.color.panel=function(colors, ncols=256)
{  
  panel = NULL
  for (i in 2:length(colors))
    panel = c(panel, colorpanel(ncols, colors[i-1], colors[i]))
  panel
}

# for example vals.to.cols(1:10, c(1, 3, 10), ncols=10) returns:
# [1] 1  6 11 12 14 15 16 17 19 20
vals.to.cols=function(vals, breaks, ncols=256)
{
  min = breaks[1]
  max = breaks[length(breaks)]
  n = length(breaks)-1
  cols = rep(-1, length(vals))
  for (i in 1:n)
  {
    ind = (breaks[i] <= vals) & (vals <= breaks[i+1])
    if (!any(ind))
      next
    # normalize to [0,1]
    cols[ind] = (vals[ind] - breaks[i]) / (breaks[i+1] - breaks[i])
    # normalize to [i*ncols,i*(ncols+1)]
    cols[ind] = (i-1)*ncols + cols[ind]*(ncols-1) + 1
    # round
    cols[ind] = round(cols[ind])
  }
  return (cols)
}

plot.colored.matrix=function(x, y, size, values, breaks=NULL, ncols=256, border=NA, auto.axis=F,
  labels=NA, xlabels=NA, ylabels=NA, at=NA, at.x=NA, at.y=NA, colors=c("blue", "cyan", "green", "yellow", "orange", "red"), xlab="", ylab="", colnote.from.num=NA, nums.col='black', nums.cex=1, main="", na.col="lightgrey",y.axis.side=2, x.axis.side=1, asp=1, start.new.plot=T)
{

  if (start.new.plot) {
    plot.new()
    plot.window(xlim=range(c(at.x, x) - size, c(at.x,x)+size), ylim=range(c(at.y, y)-size, c(at.y, y)+size), asp=asp)
  }
  
  if (!any(is.na(at.x))) {
    axis(x.axis.side, at=at.x, labels=xlabels, las=2)
  } else if (auto.axis) {
    axis(x.axis.side)
  }

  if (!any(is.na(at.y))) {
    axis(y.axis.side, at=at.y, labels=ylabels, las=2)
  } else if (auto.axis) {
    axis(y.axis.side)
  }

  if (main != "")
    title(main=main)
  
  if (xlab != "")
    title(xlab=xlab)

  if (ylab != "")
    title(ylab=ylab)

  # temporarily put values instead of NAs
  na.ind = is.na(values)
  values[na.ind] = min(c(na.omit(values), 0))
  if (is.null(breaks)) 
    breaks = seq(min(values), max(values), length.out=length(colors))
  min.value = breaks[1]
  max.value = breaks[length(breaks)]
  if (length(breaks) != length(colors))
    stop("#breaks and #colors must be equal")
  values[na.ind] = min.value

  # report on trimmed values
  if (min(values) < min.value || max(values) > max.value)
  {
    cat(sprintf("Warning: trimming range (%.1f,%.1f) into range (%.1f, %.1f)\n",
                min(values), max(values), min.value, max.value))
    indices = which(values > max.value | values < min.value)
    if (length(indices) < 10)
    for (ind in indices)
      cat(sprintf("  (%.1f, %.1f): %.1f\n", x[ind], y[ind], values[ind]))
  }
  
  # trim values
  values[values < min.value] = min.value
  values[values > max.value] = max.value

  # make color panel
  panel = make.color.panel(colors, ncols)
  
  # get color indices
  col = vals.to.cols(values, breaks, ncols)

  if (!all(is.finite(col)))
    stop("missing values")

  # set colors
  rect.col = panel[col]
  
  # set NA colors
  rect.col[na.ind] = na.col

  rect(x-size, y-size, x+size, y+size, col=rect.col, border=border)
  if (!is.na(colnote.from.num) & sum(values >= colnote.from.num) > 0) {
    ind = values >= colnote.from.num
    text(x[ind], y[ind], labels=values[ind], col=nums.col, cex=nums.cex)
  }
}

plot.colored.matrix.legend=function(values=1000*1:10, breaks=c(2,7,10)*1000, ncols=512, equal.spaced=F,
                                    colors=c("green", "red", "blue"),
                                    width=0.1, height=2, rotate=F, name="", leg.cex=1)
{
  if (is.null(breaks))
    breaks = seq(min(na.omit(values)), max(na.omit(values)), length.out=length(colors))

  min.value = breaks[1]
  max.value = breaks[length(breaks)]
  if (length(breaks) != length(colors))
    stop("#breaks and #colors must be equal")
  
  # make color panel
  panel = make.color.panel(colors, ncols)
  
  nlegend = 100
  legend.values = seq(min.value, max.value, (max.value - min.value) / (nlegend-1))
  legend.cols = vals.to.cols(legend.values, breaks, ncols)

  par(mai=c(0.1, 0.1, 0.1, 0.1))
  plot.new()
  fin = par("fin")

  if (rotate) {
    plot.width = fin[2]
    plot.height = fin[1]
  } else {
    plot.width = fin[1]
    plot.height = fin[2]
  }

  # x axis
  xlim=c(0,1)
  offset.x = ((plot.width - width)/2) / plot.width
  if (offset.x < 0)
    stop("legend too wide") 
  left = offset.x
  right = 1 - offset.x
  
  # y axis
  ylim = c(min.value, max.value)
  delta.y = ylim[2]-ylim[1]
  ratio = delta.y / height
  offset.y = ratio * (((plot.height - height)/2))
  if (offset.y < 0)
    stop("legend too high") 
  ylim[1] = ylim[1] - offset.y
  ylim[2] = ylim[2] + offset.y

  if (rotate)
    plot.window(xlim=ylim, ylim=xlim)
  else
    plot.window(xlim=xlim, ylim=ylim)

  if (name != "") {
    #title(name)
    if (rotate)
      text(y=(left+right)/2, x=legend.values[1], labels=name, pos=2)
    else
      text(x=left, y=legend.values[1], labels=name, pos=1)
  }
   
  rect.col = panel[legend.cols]

  y = legend.values
  y.bottom = y[1:(length(y)-1)]
  y.top = y[2:length(y)]

  if (rotate)
    rect(y.bottom, left, y.top, right, col=rect.col, border=NA)
  else
    rect(left, y.bottom, right, y.top, col=rect.col, border=NA)
  
  if (equal.spaced)
    axis.values = seq(min.value, max.value,length.out=4)
  else
    axis.values = breaks
  tick.size = 0.07 / plot.width

  if (rotate) {
    segments(axis.values, right, axis.values, right+tick.size)
    text(axis.values, right+tick.size, axis.values, pos=3, cex=leg.cex)
  } else {
    segments(right, axis.values, right+tick.size, axis.values)
    text(right+tick.size, axis.values, axis.values, pos=4, cex=leg.cex)
  }
}

# wrapper function
plot.smatrix=function(smat, size=0.5, visitor.f=NULL,
  title="", fn="", width=5, height=4,
  mai.matrix=c(0.7, 0.5, 0.4, 0.1),
  mai.legend=c(0.5, 0.1, 0.5, 0.6),
  legend.width=1.2,
  breaks=NULL, ncols=256, border=NA, auto.axis=F, labels=NA, at=NA, equal.spaced=F,
  xlabels=NA, at.x=NA, ylabels=NA, at.y=NA,
  colors=c("blue", "cyan", "green", "yellow", "orange", "red"), discard.diag=F, leg.cex=1, xlab="", ylab="", leg.lab="")
{
  
  if (fn != "") {
    start.plot(fn, width=width, height=height, device="postscript")
    layout(matrix(c(1,1,2,2),2), width=c(width-legend.width,legend.width))
  }


  if (!any(is.na(labels))) {
    cat("Using labels for both axis\n")
    xlabels = labels
    ylabels = labels
    at.x = at
    at.y = at
  }
    
  plot.new()
  op = par(mai=mai.matrix)
  title(title)

  x = smat[,1]
  y = smat[,2]
  values = smat[,3]

  xlim=range(c(x+size, x-size))
  ylim=range(c(y+size, y-size))

  plot.window(xlim=xlim, ylim=ylim)
  
  plot.colored.matrix(x=x, y=y, size=size, auto.axis=auto.axis, xlabels=xlabels, at.x=at.x, ylabels=ylabels, at.y=at.y, 
                      values=values, breaks=breaks, ncols=ncols, colors=colors, border=border, xlab=xlab, ylab=ylab)
  
  if (!is.null(visitor.f))
    visitor.f()
  par(mai=mai.legend)

  plot.colored.matrix.legend(values=values, breaks=breaks, ncols=ncols, colors=colors, equal.spaced=equal.spaced, leg.cex=leg.cex, name=leg.lab)

  if (fn != "") {
    end.plot(fn)
  }
  
  par(op)
}
