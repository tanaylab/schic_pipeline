#########################################################################################################
# Spatial 2D plots
#########################################################################################################

# type = "o" (observed) "e" (expected) or "n" (normalized)
spatial.plot=function(ifn.prefix="results/E1a", dataset="E1a", type="n",
                                chrs.xlimit=NULL, chrs.ylimit=NULL, chroms=c(1:19, 'X'), ofn.prefix="",
                                fdir="figures/spatial/", binsize=1*10^6,
                                colors=c("blue", "white", "yellow", "red", "orange", "purple"), breaks=NULL,
                                use.coords=T, raw=F, normalize.marginals=F, nm=F, width=7.5, plot.cutoff=T, plot.legend=T, main.str="")
{
  
#  if (binsize >= 10^6) bin.str = paste(binsize/10^6, "M", sep="") else  bin.str = paste(binsize/10^3, "K", sep="")
  bin.str = format(binsize, sci=F)
 
  nm.str = ifelse(nm, "_nm", "")
  nstr = ifelse (normalize.marginals, "_norm", "")
  if (type == "o")
      ifn = paste(ifn.prefix, "_", bin.str, ".o_contact", sep="")
  else
    if (raw)  ifn = paste(ifn.prefix, "_", bin.str, nm.str, ".n_contact", nstr, sep="")  else  ifn = paste(ifn.prefix, "_", bin.str, nm.str, ".n_smooth", nstr, sep="")

  cat(sprintf("reading %s\n", ifn))
  
  contact = ca.get.table(ifn)
  cbins = ca.get.table(paste(ifn.prefix, "_", bin.str, ".cbins", sep=""))
  cbins$from.coord = as.numeric(cbins$from.coord)
  cbins$to.coord = as.numeric(cbins$to.coord)
  
  cbins.x = cbins
  cbins.y = cbins

  if (type != "o") {
    m = merge(contact, cbins[,c("cbin", "chr", "from.coord", "to.coord")], by.x="cbin1", by.y="cbin")
    names(m)[c((dim(m)[2]-2):dim(m)[2])] = c("chr1", "from1", "to1")
    m = merge(m, cbins[,c("cbin", "chr", "from.coord", "to.coord")], by.x="cbin2", by.y="cbin")
    names(m)[c((dim(m)[2]-2):dim(m)[2])] = c("chr2", "from2", "to2")
    names(m)[ which(names(m) == 'cbin1')] = 'coord_bin1'
    names(m)[ which(names(m) == 'cbin2')] = 'coord_bin2'
    names(m)[ which(names(m) == 'observed_count')] = 'unique_observed_count'
    contact = m

  }
    
  if (!is.null(chrs.xlimit))
  {
    contact = contact[is.element(contact$chr1, chrs.xlimit),]
    cbins.x = cbins.x[is.element(cbins.x$chr, chrs.xlimit),]
  }
  if (!is.null(chrs.ylimit))
  {
    contact = contact[is.element(contact$chr2, chrs.ylimit),]
    cbins.y = cbins.y[is.element(cbins.y$chr, chrs.ylimit),]
  }
  levels = chroms
  levels.x = levels[is.element(levels, unique(cbins.x$chr))]
  levels.y = levels[is.element(levels, unique(cbins.y$chr))]
  cbins.x = split(cbins.x, factor(cbins.x$chr, levels=levels.x))
  cbins.y = split(cbins.y, factor(cbins.y$chr, levels=levels.y))
  
  if (use.coords) {  
    chrs.x = sapply(cbins.x, function(x) {range(x$from.coord, x$to.coord)})
    chrs.y = sapply(cbins.y, function(x) {range(x$from.coord, x$to.coord)})
    if (dim(chrs.x)[2] > 1)
    for (i in 2:dim(chrs.x)[2]) {
      chrs.x[,i] = chrs.x[,i] + chrs.x[2,i-1]
      contact[contact$chr1 == colnames(chrs.x)[i], c("from1", "to1")] =
        contact[contact$chr1 == colnames(chrs.x)[i], c("from1", "to1")] + chrs.x[2,i-1]
    }
    if (dim(chrs.y)[2] > 1)
    for (i in 2:dim(chrs.y)[2]) {
      chrs.y[,i] = chrs.y[,i] + chrs.y[2,i-1]
      contact[contact$chr2 == colnames(chrs.y)[i], c("from2", "to2")] =
        contact[contact$chr2 == colnames(chrs.y)[i], c("from2", "to2")] + chrs.y[2,i-1]
    }
  }
  else {
    chrs.x = sapply(cbins.x, function(x) {range(x$cbin)})
    chrs.y = sapply(cbins.y, function(x) {range(x$cbin)})
  }
  delta = 1

  values = switch(type,
    o = contact$unique_observed_count,
    e = contact$expected_count,
    n = log2((contact$unique_observed_count + delta) / (contact$expected_count + delta)),
    z = (contact$unique_observed_count - contact$expected_count) / sqrt(contact$expected_count+1))

  if (is.null(breaks)) 
    breaks = seq(min(values), max(values), length.out=length(colors))
  min.value = breaks[1]
  max.value = breaks[length(breaks)]
  if (length(breaks) != length(colors))
    stop("#breaks and #colors must be equal")
  
  # plot cutoff
  trans.ind = contact$chr1 != contact$chr2
  cis.ind = contact$chr1 == contact$chr2
  if (any(trans.ind) && plot.cutoff)
  {
    cutoff.ind = trans.ind
    xlim = range(c(min.value, max.value, values[cutoff.ind]))
    if (fdir != "") {
      fn = paste(fdir, "cutoff/", type, "_", dataset, ".png", sep="")
      system(paste("mkdir -p", get.fn.dir(fn)))
      start.plot(fn, width=400, height=400, device="png", png.units="px")
    }
    plot(density(values[cutoff.ind]), main=paste("trans ", dataset, "_", type, sep=""), col=1, xlim=xlim)
    abline(v=c(min.value, max.value), lty=2)
    if (fdir != "")
      end.plot(fn)
  }
  
  # trim values
  values[values < min.value] = min.value
  values[values > max.value] = max.value

  # make color panel
  ncols = 256
  panel = make.color.panel(colors, ncols)
  col = vals.to.cols(values, breaks, ncols)

  legend.values = breaks
  legend.cols = vals.to.cols(legend.values, breaks, ncols)

  if (!all(is.finite(col)))
    stop("missing values")
  
  xlim = range(chrs.x)
  ylim = range(chrs.y)

  raw.str = ifelse(raw, "_raw", "")

  if (fdir != "") {
    system(paste("mkdir -p", paste(fdir, type, sep="")))
    fns = c(paste(fdir, type, "/", ofn.prefix, "_", dataset, raw.str, nstr, ".eps", sep=""),
      paste(fdir, ofn.prefix, "_", dataset, "_",type, nstr, ".eps", sep=""))
    legend.fns = c(paste(fdir, type, "/", ofn.prefix, "_", dataset, "_legend", raw.str, ".eps", sep=""),
      paste(fdir, ofn.prefix, "_", dataset, "_",type,"_legend.eps", sep=""))
    fn = fns[1]
  }
  

  height = width / ((xlim[2] - xlim[1]) / (ylim[2] - ylim[1]))
  if (fdir != "") {
    start.plot(fn, width=width, height=height)
  }
  par(mai=c(0.2, 0.9, 1.3, 0.3))
  #layout(matrix(1:2, 1, 2))

  if (fdir != "") {
    system(paste("mkdir -p", get.fn.dir(fn)))
  }
  plot.new()
  #title(paste(dataset, type, sep="_")) #, line=-2, outer=F)
  title(main.str)
  plot.window(xlim=xlim, ylim=ylim)

  # x axis
  at.x = chrs.x[1,]/2 + chrs.x[2,]/2
  labels.x = colnames(chrs.x)
  axis(3, at=at.x, labels=labels.x, tick=F, las=1, cex.axis=0.75)
  at.ticks.x = c(chrs.x[1,1]-0.5, chrs.x[2,] + 0.5)
  axis(3, at=at.ticks.x, labels=F)
  
  # y axis
  at.y = chrs.y[1,]/2 + chrs.y[2,]/2
  labels.y = colnames(chrs.y)
  axis(2, at=at.y, labels=labels.y, tick=F, las=2, cex.axis=0.75)
  at.ticks.y = c(chrs.y[1,1]-0.5, chrs.y[2,] + 0.5)
  axis(2, at=at.ticks.y, labels=F)


  # pixels
  if (use.coords) {
    rect(contact$from1, contact$from2, contact$to1, contact$to2, col=panel[col], border=NA)
    rect(contact$from2, contact$from1, contact$to2, contact$to1, col=panel[col], border=NA)
  } else {
    rect(contact$cbin1-0.5, contact$cbin2-0.5, contact$cbin1+0.5, contact$cbin2+0.5, col=panel[col], border=NA)
    rect(contact$cbin2-0.5, contact$cbin1-0.5, contact$cbin2+0.5, contact$cbin1+0.5, col=panel[col], border=NA)
  }

  # grid
  segments(x0=at.ticks.x, y0=rep(ylim[1], length(at.ticks.x)),
           x1=at.ticks.x, y1=rep(ylim[2], length(at.ticks.x)))
  segments(y0=at.ticks.y, x0=rep(xlim[1], length(at.ticks.y)),
           y1=at.ticks.y, x1=rep(xlim[2], length(at.ticks.y)))

  if (fdir != "") {
    end.plot(fn)
    
    # copy result
    for (i in 2:length(fns))
    {  
      system(paste("mkdir -p", get.fn.dir(fns[i])))
      system(paste("cp ", fn, fns[i]))
    }
  }

  # legend
  if (plot.legend) {
    if (fdir != "") {
      fn = legend.fns[1]
      start.plot(fn, width=2, height=2)
    }
    
    par(mai=c(0, 0, 0, 0))
    plot.new()
    plot.window(c(0, 1), c(0, 1))
    legend(0, 0.2, horiz=F, fill=panel[legend.cols], legend=round(legend.values, 2),
           xjust=0, yjust=0)
    
    if (fdir != "") {
      end.plot(fn)
      
                                        # copy result
      for (i in 2:length(legend.fns))
        {  
          system(paste("mkdir -p", get.fn.dir(legend.fns[i])))
          system(paste("cp ", fn, legend.fns[i]))
        }
    }
  }
}

################
make.color.panel=function(colors, ncols=256)
{  
  panel = NULL
  for (i in 2:length(colors))
    panel = c(panel, colorpanel(ncols, colors[i-1], colors[i]))
  panel
}

############
vals.to.cols=function(vals, breaks, ncols=256)
{
  min = breaks[1]
  max = breaks[length(breaks)]
  n = length(breaks)-1
  cols = rep(-1, length(vals))
  for (i in 1:n)
  {
    ind = (breaks[i] <= vals) & (vals <= breaks[i+1])
    if (!any(ind))
      next
    # normalize to [0,1]
    cols[ind] = (vals[ind] - breaks[i]) / (breaks[i+1] - breaks[i])
    # normalize to [i*ncols,i*(ncols+1)]
    cols[ind] = (i-1)*ncols + cols[ind]*(ncols-1) + 1
    # round
    cols[ind] = round(cols[ind])
  }
  return (cols)
}


#########################################################################################################
# Plotting functions
#########################################################################################################

cmap.general=function(
  clean = F,
  datasets = c("HiC", "combined_TH1_bgl", "combined_TH1_dpn"),
  cis.models = c(F, F, F),
  odir="results",
  fdir = "figures/cmaps/",
  title = "test",
  binsizes = c(0.25,0.5,1,2,4,8,16)*10^6,
  type = c("o"),
  chrs.limit = NULL,
  chrs.limit.other = NULL,
  #coord.limit = c(80,100)*10^6,
  coord.limit = NULL,
  coord.limit.other = NULL,
  #max.dist = 50*10^6,
  max.dist = NULL,
  diagonal = F,
  raw = T,
  profile.style="full",
  profile.size.factor = 1,
  main.size.x = NULL,
  profiles = NULL,
  iterator = NULL,
  #map.visitor = domain.border.visitor
  map.visitor = NULL,
  min.observed=8,
  normalize.marginals = F,
  global.breaks=NULL,
  map.layer=NULL,
  colors=c("white", "darkred", "purple", "black"),
  breaks=c(0, 4, 8, 20),
  chrs.len.ifn="input/mm9_chroms.txt")
{
  source("./R/project.r")

  context = paste("cmap.general")
  ca.context(context=context, clean=clean)
  
  options(gparam.type="string")
  
  if (is.null(main.size.x)) {
    main.size.x = if (diagonal) 13 else 12
  }
  
  diagonal.band.ratio = 0.75

  coord.xlimit = coord.limit
  if (is.null(coord.limit.other)) coord.ylimit = coord.limit else coord.ylimit = coord.limit.other

  chrs.xlimit = chrs.limit
  if (is.null(chrs.limit.other)) chrs.ylimit = chrs.xlimit else chrs.ylimit = chrs.limit.other

  if (is.null(iterator))
    iterator = diff(coord.xlimit) / 1000
  
  cat(sprintf("default iterator used for profiles: %f\n", round(iterator)))
  cat(sprintf(">>> Plotting %s\n", type))

  #cat(sprintf("preparing profiles\n"))
    
  for (i in 1:length(datasets)) {
    dataset = datasets[i]

    bands = NULL
  
    ifn.prefix = paste(odir, "/", dataset, sep="")
    cmap(ifn.prefix=ifn.prefix, fdir=fdir, dataset=dataset, type=type,
         ofn.prefix=title, colors=colors, breaks=breaks,
         chrs.xlimit=chrs.xlimit, chrs.ylimit=chrs.ylimit,
         coord.xlimit=coord.xlimit, coord.ylimit=coord.ylimit,
         binsizes=binsizes, profiles=profiles, legend.height=2.5,
         raw=raw, normalize.marginals=normalize.marginals, 
         diagonal=diagonal, bands=bands, main.size.x=main.size.x,
         diagonal.band.ratio=diagonal.band.ratio, max.dist=max.dist,
         profile.size.factor=profile.size.factor, profile.iterator=iterator,
         min.observed=min.observed, map.visitor=map.visitor, map.layer=map.layer, chrs.len.ifn=chrs.len.ifn)
  }
}

# compare two areas, show matrix as is
cmap.layout.ortho=function(title.height=0.7, legend.width=2,
                           main.size.x=5, main.size.y=3, profile.sizes=c(1,1,1))
{
  N = length(profile.sizes)

  if (N == 0)
  {
    m = matrix(c(1,2,3,2),2,2)
    heights = c(title.height, main.size.y)
    widths = c(main.size.x-legend.width, legend.width)
    return (list(heights=heights, widths=widths, m=m))
  }
  
  m = matrix((N+1)*2+1, N+1, N+1)

  # set profile panels
  m[,1] = c(1:N*2,1)
  m[N+1,] = c(1,(N:1)*2+1)

  # add title+legend column
  m = rbind(rep(1, N+1), m+1)
  
  # add legend row
  m = cbind(m, c((N+1)*2+1, m[2:(N+2),N+1]))
  
  heights = c(title.height, c(profile.sizes), main.size.y)
  widths = c(main.size.x, rev(profile.sizes[2:N]), profile.sizes[1]-legend.width, legend.width)

  return (list(heights=heights, widths=widths, m=m))
}

# cis interactions, show rotated matrix
cmap.layout.rotated=function(title.height=0.7, legend.width=2, xaxis.margin=0.5,
                             main.size.x=5, main.size.y=3, profile.sizes=c(1,1,1))
{
    N = length(profile.sizes)

    if (N == 0)
    {
      m = matrix(c(1,2,3,2),2,2)
      heights = c(title.height, main.size.y)
      widths = c(main.size.x-legend.width, legend.width)
      return (list(heights=heights, widths=widths, m=m))
    }
    
    m = matrix(c(1,3:(N+2),2), N+2, 1)
    m = cbind(m,m)
    m[1,2] = max(m)+1

    # add margin on top for x-axis
    profile.sizes[1] = profile.sizes[1] + xaxis.margin
    
    heights = c(title.height, profile.sizes, main.size.y)
    widths = c(main.size.x-legend.width, legend.width)

    return (list(heights=heights, widths=widths, m=m))
}

cmap=function(
  ifn.prefix="results/wtb_s0", dataset="wtb_s0", type="n",
  chrs.xlimit="3R", chrs.ylimit="3R",
  coord.xlimit=NULL, coord.ylimit=NULL, 
  ofn.prefix="", plot.to.screen=F,
  fdir="figures/cmaps/", binsizes=5*10^3,
  colors=c("blue", "cyan", "white", "orange", "red", "purple", "black", "yellow"),
  breaks=c(-2,-0.5,0,0.5,2,6,8,12), plot.legend=T,
  profiles=NULL, title="", 
  raw=T, normalize.marginals=F, legend.height=3,
  diagonal=T, bands=NULL, main.size.x=6,
  diagonal.band.ratio=0.25, max.dist=NULL, profile.size.factor=1,
  profile.iterator=1000, min.observed=NULL, map.visitor=NULL,
  map.layer=NULL, chrs.len.ifn="input/mm9_chroms.txt")
{
#  gsetroot("/net/mraid04/export/data/db/tgdb/mm9/trackdb/")
#  options(gparam.type="string")
  
  norm.str = ifelse (normalize.marginals, "_norm", "")
  raw.str = ifelse (raw, "_raw", "")
  diagonal.str = ifelse (diagonal, "", "_ortho")
  ofn.prefix.str = if (ofn.prefix != "") paste(ofn.prefix, "_", sep="") else ""
  ofn = paste(fdir, ofn.prefix.str, dataset, "_", type, norm.str, diagonal.str, ".eps", sep="")

  contact.maps = list()
  binsizes = sort(binsizes, decreasing=T)
  cat(sprintf("getting contact maps: %s\n", paste(binsizes, collapse=",")))
  for (i in seq_along(binsizes)) {
    binsize = binsizes[i]
    contact = get.contact.table(
      ifn.prefix=ifn.prefix, 
      chrs.xlimit=chrs.xlimit, chrs.ylimit=chrs.ylimit,
      coord.xlimit=coord.xlimit, coord.ylimit=coord.ylimit,
      max.dist=max.dist,
      binsize=binsize, raw=raw,
      normalize.marginals=normalize.marginals, type=type)

    if (!is.null(max.dist))
      contact = contact[abs(contact$from1 - contact$from2) <= max.dist+2*binsize,]

    if (!is.null(min.observed))
      contact = contact[contact$unique_observed_count > min.observed, ]
    
    contact.maps[[i]] = contact
  }
  cat(sprintf("getting contact maps done\n"))

  cmap.internal(contact.maps=contact.maps, binsizes=binsizes,
                coord.xlimit=coord.xlimit, coord.ylimit=coord.ylimit, 
                dataset=dataset, type=type, 
                ofn.prefix=ofn.prefix, ofn=ofn, plot.to.screen=plot.to.screen,
                colors=colors, breaks=breaks, profiles=profiles, title=title,
                legend.height=legend.height, plot.legend=plot.legend,
                diagonal=diagonal, bands=bands, main.size.x=main.size.x,
                diagonal.band.ratio=diagonal.band.ratio, max.dist=max.dist,
                profile.size.factor=profile.size.factor, profile.iterator=profile.iterator,
                map.visitor=map.visitor, map.layer=map.layer, chrs.len.ifn=chrs.len.ifn)
}

coord.axis=function(side, factor=1, las=1, label="")
{  
  axis(side, las=2, labels=F)
  at = axTicks(side)
  labels = paste(factor*at/1000, "k", sep="")
  axis(side, las=las, at=at, labels=labels)
  if (label != "")
    mtext(text=label, side=side, las=1, line=3)
}

# get coord ranges of multiple chroms, bringing all the a unified system
get.ranges=function(chrs.ifn, chrs, round.binsize=100000)
{     
  all.chroms = read.delim(chrs.ifn, header=F)
  all.chroms = all.chroms[ is.element(all.chroms[,1], chrs) ,]
  
  result = matrix(nrow=2, ncol=length(chrs))
  for (i in seq_along(chrs))
  {
    start.coord = 0
    end.coord = all.chroms[all.chroms[,1] == chrs[i], 2]
    end.coord = ceiling(end.coord/round.binsize)*round.binsize
    result[,i] = c(start.coord, end.coord)
    if (i > 1)
      result[,i] = result[,i] + result[2,i-1]
  }
  colnames(result) = chrs
  result
}

unify.coords=function(contact, chr.ranges.x, chr.ranges.y)
{  
  if (dim(chr.ranges.x)[2] > 1)
    for (i in 2:dim(chr.ranges.x)[2]) {
      contact[contact$chr1 == colnames(chr.ranges.x)[i], c("from1", "to1")] =
        contact[contact$chr1 == colnames(chr.ranges.x)[i], c("from1", "to1")] + chr.ranges.x[2,i-1]
    }
  if (dim(chr.ranges.y)[2] > 1)
    for (i in 2:dim(chr.ranges.y)[2]) {
      contact[contact$chr2 == colnames(chr.ranges.y)[i], c("from2", "to2")] =
        contact[contact$chr2 == colnames(chr.ranges.y)[i], c("from2", "to2")] + chr.ranges.y[2,i-1]
    }
  contact
}

get.contact.values=function(contact, breaks, type)
{
  delta = 0
  if (is.element("expected_count", colnames(contact)))
    delta = min(contact$expected_count)/10
  
  if (!is.element("unique_observed_count", colnames(contact))) {
    contact$unique_observed_count = contact$observed_count
  }
  
  values = switch(type,
    o = contact$unique_observed_count,
    e = contact$expected_count,
    r = contact$ratio,
    n = log2((contact$unique_observed_count + delta) / (contact$expected_count + delta)),
    z = (contact$unique_observed_count - contact$expected_count) / sqrt(contact$expected_count+1))

  
  # trim values
  min.value = breaks[1]
  max.value = breaks[length(breaks)]
  values[values < min.value] = min.value
  values[values > max.value] = max.value
  
  values
}

get.contact.colors=function(contact, breaks, colors, type, panel, ncols)
{
  values = get.contact.values(contact, breaks, type)
  
  if (length(breaks) != length(colors))
    stop("#breaks and #colors must be equal")

  # convert values to colors

  col = vals.to.cols(values, breaks, ncols)
  if (!all(is.finite(col)))
    stop("missing values")

  panel[col]
}

cmap.internal=function(
  contact.maps=NULL,
  coord.xlimit=NULL, coord.ylimit=NULL, 
  binsizes=binsizes,
  dataset="wtb_s0", type="n", 
  ofn.prefix="", ofn="figures/test.eps", plot.to.screen=F,
  colors=c("blue", "cyan", "white", "orange", "red", "purple", "black", "yellow"),
  breaks=c(-2,-0.5,0,0.5,2,6,8,12),
  profiles=NULL, title="", plot.legend=T,
  legend.height=2,
  diagonal=T, bands=NULL, diagonal.band.ratio=0.25, max.dist=NULL,
  main.size.x=6, profile.size.factor=1,
  profile.iterator=1000, map.visitor=NULL, map.layer=NULL, chrs.len.ifn="input/mm9_chroms.txt")
{
  #cat(sprintf("starting cmap.internal\n"))
  if (is.null(contact.maps))
    stop("no contacts defined")
  
  # prepare color panel
  ncols = 256
  panel = make.color.panel(colors, ncols)

  # get coord ranges, on a unified coord system
  first.contact = contact.maps[[1]]

  chrs1 = first.contact$chr1
  chrs2 = first.contact$chr2

  sort.num.then.str=function(u) {
    suppressWarnings(c(sort(as.numeric((u[!is.na(as.numeric(u))]))) , sort(u[is.na(as.numeric(u))])))
  }
  chr.ranges.x = get.ranges(chrs.ifn=chrs.len.ifn, chrs=sort.num.then.str(unique(chrs1)))
  chr.ranges.y = get.ranges(chrs.ifn=chrs.len.ifn, chrs=sort.num.then.str(unique(chrs2)))

  first.contact = unify.coords(first.contact, chr.ranges.x, chr.ranges.y)
  if (!is.null(coord.xlimit)) xlim = coord.xlimit
  else xlim = range(first.contact[,c("from1", "to1")])
  if (!is.null(coord.ylimit)) ylim = coord.ylimit
  else ylim = range(first.contact[,c("from2", "to2")])

  if (diagonal) {
    if (is.null(max.dist)) {
      coord.width = xlim[2]-xlim[1]
      max.dist = coord.width * diagonal.band.ratio
    }
    ylim = c(-max.dist/2, 0)
  }
  
  ###########################################################################
  # prepare layout

  ratio = diff(ylim) / diff(xlim)
  
  title.height = 0.7
  legend.width = 3
  main.size.y = main.size.x * ratio
  xaxis.margin = 0.3

  if (length(profiles)>0) {
    sizes = numeric(length(profiles))
    for (i in 1:length(profiles))
      sizes[i] = profiles[[i]]$plot.size * profile.size.factor
  } else {
    sizes = NULL
    main.size.y = main.size.y + xaxis.margin # the map is in charge of the axis
  }
  
  if (!diagonal) {
    layout = cmap.layout.ortho(title.height=title.height, legend.width=legend.width,
      main.size.x=main.size.x, main.size.y=main.size.y, profile.sizes=sizes)
  } else {
    layout = cmap.layout.rotated(title.height=title.height, legend.width=legend.width,
      main.size.x=main.size.x, main.size.y=main.size.y, profile.sizes=sizes, xaxis.margin=xaxis.margin)
  }

  ###########################################################################
  # start plot

  if (!plot.to.screen)
    system(paste("mkdir -p", get.fn.dir(ofn)))
  else
    ofn = ""
  start.plot(ofn, width=sum(layout$widths), height=sum(layout$heights))
  layout(layout$m, heights=layout$heights, widths=layout$widths)
  
  ###########################################################################
  # plot title

  single.chrom = (dim(chr.ranges.y)[2] == 1 && dim(chr.ranges.x)[2] == 1 && colnames(chr.ranges.x)==colnames(chr.ranges.y))
  chr.str = if (single.chrom) colnames(chr.ranges.x) else chr.str = ""

  par(mai=c(0,0,0,0))
  plot.new()
  plot.window(xlim=0:1, ylim=0:1)
  text(x=0.5, y=0.5, paste(title, dataset, type, chr.str), cex=1.5/par("cex"))
  
  ###########################################################################
  # plot contact map
  
  par(xaxs="i", yaxs="i")
  
  if (diagonal) {
    main.mai = c(0.1, 0.9, 0, 0.1)
    # special case w/o profiles
   if (length(profiles) == 0)
     main.mai = c(0.1, 0.9, 0.3, 0.1)
  } else {
    main.mai = c(0.9, 0.9, 0.1, 0.1)
  }
  par(mai=main.mai)
  plot.new()
  plot.window(xlim=xlim, ylim=ylim)
  if (diagonal) title(ylab="dist (kb)")

  # plot background layer if supplied
  if (!is.null(map.layer))
  {
    if (diagonal) {
      # not supported!
    }
    else {
      map.layer(chr.ranges=chr.ranges.x, chr.lim=xlim, other.chr.lim=ylim, mode="x")
      map.layer(chr.ranges=chr.ranges.y, chr.lim=ylim, other.chr.lim=xlim, mode="y")
    }
  }
  
  # xy axis
  if (!diagonal)
  {
    coord.labels = (dim(chr.ranges.y)[2] == 1 & dim(chr.ranges.x)[2] == 1)
    if (coord.labels) {
      coord.axis(1, label=colnames(chr.ranges.x)[1])
      coord.axis(2, label=colnames(chr.ranges.y)[1])
    } else {
      # x axis
      at.x = chr.ranges.x[1,]/2 + chr.ranges.x[2,]/2
      labels.x = colnames(chr.ranges.x)
      at.ticks.x = c(chr.ranges.x[1,1]-0.5, chr.ranges.x[2,] + 0.5)
      axis(1, at=at.ticks.x, labels=F)
      axis(1, at=at.x, labels=labels.x, tick=F, las=1, cex.axis=0.75)
      
      # y axis
      at.y = chr.ranges.y[1,]/2 + chr.ranges.y[2,]/2
      labels.y = colnames(chr.ranges.y)
      at.ticks.y = c(chr.ranges.y[1,1]-0.5, chr.ranges.y[2,] + 0.5)
      axis(2, at=at.ticks.y, labels=F)
      axis(2, at=at.y, labels=labels.y, tick=F, las=2, cex.axis=0.75)
    }
  }

  # plot all contact maps one on top of the other
  cat(sprintf("plotting contact maps\n"))
    
  for (j in 1:length(contact.maps)) {
    contact = contact.maps[[j]]
    if (dim(contact)[1] == 0)
      next
    
    # switch to global coord system
    contact = unify.coords(contact, chr.ranges.x, chr.ranges.y)
    xleft = contact$from1
    ybottom = contact$from2
    xright = contact$to1
    ytop = contact$to2

    # get colors
    col = get.contact.colors(contact, breaks, colors, type, panel, ncols)
  
    if (!diagonal) {
      # draw colored rects
      rect(xleft, ybottom, xright, ytop, col=col, border=NA)
      rect(ybottom, xleft, ytop, xright, col=col, border=NA)
    } else {
      s2 = 2
      xx = NULL
      yy = NULL
      polcol = NULL

      for (i in 1:length(xleft)) {
        if (xleft[i] > ybottom[i])
          next
        xx = c(xx, NA, (xleft[i]+ybottom[i])/s2, (xleft[i]+ytop[i])/s2, (xright[i]+ytop[i])/s2, (xright[i]+ybottom[i])/s2)
        yy = c(yy, NA, (xleft[i]-ybottom[i])/s2, (xleft[i]-ytop[i])/s2, (xright[i]-ytop[i])/s2, (xright[i]-ybottom[i])/s2)
        polcol = c(polcol, col[i])
      }
      polygon(x=xx, y=yy, col=polcol, border=NA)
    }
    abline(h=0)
  }
  
  # handle visitor after plotting
  if (single.chrom) {
    if (!is.null(map.visitor)) {
      if (!diagonal) {
        map.visitor(chr=colnames(chr.ranges.x)[1], xlim=xlim, mode="x")
        map.visitor(chr=colnames(chr.ranges.y)[1], xlim=ylim, mode="y")
      } else {
        map.visitor(chr=colnames(chr.ranges.x)[1], xlim=xlim, mode="xy")
      }
    }
  } else {
    grid()
  }
  
  if (!diagonal) {
    # add line that separate chroms
    if (length(colnames(chr.ranges.x)) != 1)
      segments(x0=at.ticks.x, y0=rep(ylim[1], length(at.ticks.x)),
               x1=at.ticks.x, y1=rep(ylim[2], length(at.ticks.x)))
    if (length(colnames(chr.ranges.y)) != 1)
      segments(y0=at.ticks.y, x0=rep(ylim[1], length(at.ticks.y)),
               y1=at.ticks.y, x1=rep(ylim[2], length(at.ticks.y)))
  } else {
    # add y axis
    coord.axis(side=2, factor=-2, las=2)
    
    if (!is.null(bands))
      abline(h=-bands/2)

    if (length(profiles) == 0)
      coord.axis(3)
  }

  ###########################################################################
  # plot profiles
  
  cat(sprintf("plotting profiles\n"))
  if (length(profiles) > 0)
  for (i in 1:length(profiles))
  {
    profile = profiles[[i]]

    if (profile$type == "track") {
      plot.func = plot.profile
    } else if (profile$type == "domain.borders") {
      plot.func = plot.domain.borders
    } else if (profile$type == "domain.body") {
      plot.func = plot.domain.body
    } else {
      stop("unkown profile type")
    }
    
    top = 0.01
    
    iterator = profile.iterator
    if (profile$native.iterator)
      iterator = NULL
    
    # add axis on top of first profile 
    if (i == 1 && diagonal)
      top = top + xaxis.margin
    
    pmai = list(bottom=0.02, top=top)
    par(mai=c(pmai$bottom, main.mai[2], pmai$top, main.mai[4]))

    # x profile
    for (chr in colnames(chr.ranges.x)[1])
      plot.func(profile=profile, chr=chr, xlim=xlim, rotate=F, iterator=iterator)

    # add x-axis
    if (i == 1 && diagonal) {
      if (single.chrom) label = colnames(chr.ranges.x)[1]
      else label = ""
      coord.axis(3, label=label)
    }

    # with diagonal plot only one side
    if (diagonal)
      next
    
    # y profile
    par(mai=c(main.mai[1], pmai$bottom, main.mai[3], pmai$top))
    for (chr in colnames(chr.ranges.y)[1])
      plot.func(profile=profile, chr=chr, xlim=ylim, rotate=T, iterator=iterator)
  }
  cat(sprintf("plotting profiles done\n"))
  ###########################################################################
  # color legend
  
  if (plot.legend) {
    values = get.contact.values(contact.maps[[1]], breaks, type)
    plot.colored.matrix.legend(values, colors=colors, breaks=breaks,
                               height=legend.height, rotate=T)
  }
  cat(sprintf("legend done\n"))
  
  end.plot(ofn)
  return(list(width=sum(layout$widths), height=sum(layout$heights)))
}

#########################################################################################################
# contact map
#########################################################################################################

get.contact.table=function(
  ifn.prefix="results/wtb_s0", 
  chrs.xlimit="3R", chrs.ylimit="3R",
  coord.xlimit=NULL, coord.ylimit=NULL, max.dist=NULL,
  binsize=5*10^3, raw=T, normalize.marginals=F, type="o")
{
#  bin.str = if (binsize >= 10^6)
#    paste(binsize/10^6, "M", sep="")
#  else if (binsize >= 3*10^3) paste(binsize/10^3, "K", sep="") else binsize
  bin.str = format(binsize, sci=F)
  
  norm.str = ifelse (normalize.marginals, "_norm", "")
  if (raw)
    ifn = paste(ifn.prefix, "_", bin.str, ".", type, "_contact", norm.str, sep="")
  else
    ifn = paste(ifn.prefix, "_", bin.str, ".", type, "_smooth", norm.str, sep="")

  cbins = ca.get.table(paste(ifn.prefix, "_", bin.str, ".cbins", sep=""))
  cbins$from.coord = as.numeric(cbins$from.coord)
  cbins$to.coord = as.numeric(cbins$to.coord)
  
  # limit to chroms
  limit.to.chrom = function(cbins, chrs) {
    if (is.null(chrs)) cbins
    else if (length(chrs) == 1)
      cbins[cbins$chr == chrs,]
    else
      cbins[is.element(cbins$chr, chrs),]
  }
  cbins.x = limit.to.chrom(cbins, chrs.xlimit)
  cbins.y = limit.to.chrom(cbins, chrs.ylimit)

  get.contact.table.internal = function(ifn, cbins, cbins.x, cbins.y) {
    cat(sprintf("caching table: %s\n", ifn))
    contact = read.delim(ifn)

    # cludge: special case where column names need to be fixed
    for (i in 1:2) {
      from.field = paste("coord_bin", i, sep="")
      to.field = paste("cbin", i, sep="")
      ind = which(from.field == names(contact))
      if (length(ind) == 1)
        names(contact)[ind] = to.field
    }
    # limit to bin range
#    contact = contact[contact$cbin1 >= range(cbins.x$cbin)[1] & contact$cbin1 <= range(cbins.x$cbin)[2] &

    contact = contact[is.element(contact$cbin1, cbins.x$cbin) & is.element(contact$cbin2,  cbins.y$cbin),]
    
    # append missing fields
    contact$chr1 = cbins[contact$cbin1, "chr"]
    contact$from1 = cbins[contact$cbin1, "from.coord"]
    contact$to1 = cbins[contact$cbin1, "to.coord"]
    contact$chr2 = cbins[contact$cbin2, "chr"]
    contact$from2 = cbins[contact$cbin2, "from.coord"]
    contact$to2 = cbins[contact$cbin2, "to.coord"]
    return(contact)
  }

  contact = ca.get(paste("get.contact.table.internal", ifn, paste(chrs.xlimit, chrs.ylimit, collapse=",")),
                   get.contact.table.internal(ifn, cbins, cbins.x, cbins.y))

  ## contact2 = ca.get(paste("get.contact.table.internal", ifn, paste(chrs.ylimit, chrs.xlimit, collapse=",")),
  ##   get.contact.table.internal(ifn, cbins, cbins.y, cbins.x))

  ## if (nrow(contact2) > 0) {
    
  ## }
  
  offset = if (is.null(max.dist)) 2*binsize else 2*binsize + max.dist

  # limit to coords
  if (!is.null(coord.xlimit))
    cbins.x = cbins.x[cbins.x$from.coord > coord.xlimit[1]-offset & cbins.x$to.coord < coord.xlimit[2]+offset,]
  if (!is.null(coord.ylimit))
    cbins.y = cbins.y[cbins.y$from.coord > coord.ylimit[1]-offset & cbins.y$to.coord < coord.ylimit[2]+offset,]

  # limit contact matrix
  contact = contact[contact$cbin1 >= range(cbins.x$cbin)[1] & contact$cbin1 <= range(cbins.x$cbin)[2] &
    contact$cbin2 >= range(cbins.y$cbin)[1] & contact$cbin2 <= range(cbins.y$cbin)[2],]
  
  return (contact)
}


