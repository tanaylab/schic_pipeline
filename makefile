#####################################################################################################
# Utility gmake canned recipe
#####################################################################################################

# checks return code of last shell run
define ASSERT
if [ $$? -ne 0 ]; then exit 1; fi
endef

# user defined parameters are in the cfg file
include $(CFG)

######################################################################################################
# Parameters
######################################################################################################

# output directory
ODIR?=output

# figures directory
FIGS_DIR?=$(ODIR)/figures

# Chromosomes list (tab-delimited file with chromosome names and sizes)
CHROM_SIZES?=input/mm9_chrs.txt

# cell name
DATASET?=cell5

# cell is haploid (T) or diploid (F)
HAPLOID?=F

# tag name of the first cutter used
RE1_NAME?=bgl

# fragment end tab-delimited lists - all fends, only the valid ones. Should have these columns: 
#   1. fend (fragment end id)
#   2. frag (fragment id)
#   3. strand (+/-)
#   4. chr	(chromosome name)
#   5. coord (1-based coordinate)
#   6. frag_len (fragment length)
#   7. fragend_len (re1 to re2 distance)
FENDS_DIR?=input/fends
ALL_FENDS_TABLE?=$(FENDS_DIR)/$(RE1_NAME)_all.fends
VALID_FENDS_TABLE?=$(FENDS_DIR)/$(RE1_NAME)_valid.fends

# list of non valid fends (should contain a "fend" column with the non-valid fend ids)
NON_VALID_FENDS_TABLE?=$(FENDS_DIR)/$(RE1_NAME)_non_valid.fend_ids

######################################################################################################
# Output contact maps parameters
######################################################################################################

# The following three vectors must be of the same length N>=1. They define:
# OUT_BINSIZE (bp): Squared binsize
# OUT_BINSIZE_EXP_MAX_DIST (bp): Maximal distance between bins to compute expected values for (put 0 to compute genome-wide, e.g. trans, matrices)
# OUT_N: must be set to 1,2,..,N
# Note: Binsizes 10^6 and 10^7 are required for the QC figures.
OUT_BINSIZES?=				250000		500000		1000000		2000000	4000000	5000000	8000000	10000000
OUT_BINSIZE_EXP_MAX_DISTS?=	20000000	40000000	300000000	0		0		0		0		0
OUT_N?=						1 			2 			3 			4 		5 		6 		7 		8

# Trim counts of a read-pair to MAX_COUNT when spatial binning (affects the observed_count and not the
# unique_observed_count)
MAX_COUNT?=50

######################################################################################################
# Main pipeline:
# - Check input files
# - Convert read-pairs to fend-pairs
# - For each configured bin size:
# 		- Bin fragment ends to bins
# 		- Build contact matrices for the requested bins
# - Produce QC figures and metrics
######################################################################################################

all: check_user_params
# print parameters
	@echo Output directory: $(ODIR)
	@echo Figures directory: $(FIGS_DIR)
	@echo Dataset title: $(DATASET)
	@echo First cutter name: $(RE1_NAME)
	@echo Input full fragment ends table: $(ALL_FENDS_TABLE)
	@echo Input valid fragment ends table: $(VALID_FENDS_TABLE)
	@echo Input non-valid fragment ends table: $(NON_VALID_FENDS_TABLE)
	@echo Read end length: $(READ_LEN)
	@echo Input paired reads file: $(READ_PAIRS_FN)
	@echo Haploid cell: $(HAPLOID)
	@echo Binsizes: $(OUT_BINSIZES)
	@echo /////////////////////////////////////////////////////////////////////////////////////////
	$(foreach I, $(OUT_N), make contact_map \
	                            CFG=$(CFG) \
	                            BINSIZE=$(word $(I), $(OUT_BINSIZES)) \
								OUT_BINSIZE_EXP_MAX_DIST=$(word $(I), $(OUT_BINSIZE_EXP_MAX_DISTS)); $(ASSERT); )
	make qc CFG=$(CFG)

check_user_params:
# check user parameters
	@if [ -z ${CFG} ]; then echo The config file variable CFG is not defined, see README; exit 1; fi
	@if [ -z $(ODIR) ]; then echo ODIR not defined in config file $(CFG), see README; exit 1; fi
	@if [ -z $(FIGS_DIR) ]; then echo FIGS_DIR not defined in config file $(CFG), see README; exit 1; fi
	@if [ -z $(DATASET) ]; then echo DATASET not defined in config file $(CFG), see README; exit 1; fi
	@if [ -z $(RE1_NAME) ]; then echo RE1_NAME not defined in config file $(CFG), see README; exit 1; fi
	@if [ -z $(ALL_FENDS_TABLE) ]; then echo ALL_FENDS_TABLE not defined in config file $(CFG), see README; exit 1; fi
	@if [ -z $(VALID_FENDS_TABLE) ]; then echo VALID_FENDS_TABLE not defined in config file $(CFG), see README; exit 1; fi
	@if [ -z $(NON_VALID_FENDS_TABLE) ]; then echo NON_VALID_FENDS_TABLE not defined in config file $(CFG), see README; exit 1; fi
	@if [ -z $(READ_LEN) ]; then echo READ_LEN not defined in config file $(CFG), see README; exit 1; fi
	@if [ -z $(READ_PAIRS_FN) ]; then echo READ_PAIRS_FN not defined in config file $(CFG), see README; exit 1; fi
	@if [ -z $(HAPLOID) ]; then echo HAPLOID not defined in config file $(CFG), see README; exit 1; fi
	@if [ -z "$(OUT_BINSIZES)" ]; then echo OUT_BINSIZES not defined in config file $(CFG), see README; exit 1; fi
	@if [ -z "$(OUT_BINSIZE_EXP_MAX_DISTS)" ]; then echo OUT_BINSIZE_EXP_MAX_DISTS not defined in config file $(CFG), see README; exit 1; fi
	@if [ -z "$(OUT_N)" ]; then echo OUT_N not defined in config file $(CFG), see README; exit 1; fi
# check input files
	@if [ ! -e $(ALL_FENDS_TABLE) ]; then echo Input fends table $(ALL_FENDS_TABLE) does not exist; exit 1; fi
	@if [ ! -e $(VALID_FENDS_TABLE) ]; then echo Input fends table $(VALID_FENDS_TABLE) does not exist; exit 1; fi
	@if [ ! -e $(NON_VALID_FENDS_TABLE) ]; then echo Input fends table $(NON_VALID_FENDS_TABLE) does not exist; exit 1; fi
	@if [ ! -e $(READ_PAIRS_FN) ]; then echo Input read-pairs table $(READ_PAIRS_FN) does not exist; exit 1; fi	

# remove output directory
clean: check_user_params
	rm -rf $(ODIR)
	rm -rf $(FIGS_DIR)

######################################################################################################
# PIPELINE
######################################################################################################

######################################################################################################
# Initializtion
# create directories and link dataset to fends table
######################################################################################################

$(ODIR)/%.fends:
	@echo /////////////////////////////////////////////////////////////////////////////////////////
	@echo Initialization, $*
	mkdir -p $(ODIR)
	mkdir -p $(FIGS_DIR)
	ln -sf $(abspath $(VALID_FENDS_TABLE)) $@

######################################################################################################
# Map read-pairs to fend-pairs
######################################################################################################

# input mapped read-pairs file name, columns: chr1, coord1, strand1, chr2, coord2, strand2
READ_PAIRS_FN?=input/cell5.raw

# read len
READ_LEN?=36

# Remove non amplified read-pairs (with single appearance, T/F)
REMOVE_SINGLY_AMPLIFIED?=T

$(ODIR)/%.mat: $(ODIR)/%.fends
	@echo /////////////////////////////////////////////////////////////////////////////////////////
	@echo Read-pairs to fend-pairs, $*
	$(CURDIR)/lscripts/coords2fends.pl \
		$(ALL_FENDS_TABLE) 	$(READ_PAIRS_FN)	$(READ_LEN) \
		$(ODIR)/$*.stats	$(ODIR)/$*.mat		$(DATASET) \
		T 					$(REMOVE_SINGLY_AMPLIFIED)	$(NON_VALID_FENDS_TABLE)

#####################################################################################################
# Bin fends by coordinates (.cbins file)
# Create a copy of the fend table with the bin id each fend belongs to (.cbinned file)
#####################################################################################################

# bin fends to spatial bins
$(ODIR)/shared_$(RE1_NAME)_$(BINSIZE).cbinned $(ODIR)/shared_$(RE1_NAME)_$(BINSIZE).cbins:
	$(CURDIR)/lscripts/bin_coords.pl $(VALID_FENDS_TABLE) $(ODIR)/shared_$(RE1_NAME)_$(BINSIZE).cbinned $(ODIR)/shared_$(RE1_NAME)_$(BINSIZE).cbins T $(BINSIZE)

# add binsize to dataset name
$(ODIR)/%_$(BINSIZE).cbinned $(ODIR)/%_$(BINSIZE).cbins: $(ODIR)/%.mat $(ODIR)/shared_$(RE1_NAME)_$(BINSIZE).cbinned
	@echo /////////////////////////////////////////////////////////////////////////////////////////
	@echo Linking binned dataset files, $*
	ln -sf $*.mat $(ODIR)/$*_$(BINSIZE).mat
	ln -sf $*.fends $(ODIR)/$*_$(BINSIZE).fends
	ln -sf shared_$(RE1_NAME)_$(BINSIZE).cbinned $(ODIR)/$*_$(BINSIZE).cbinned
	ln -sf shared_$(RE1_NAME)_$(BINSIZE).cbins $(ODIR)/$*_$(BINSIZE).cbins

#####################################################################################################
# Compute observed and expected binned counts
#####################################################################################################

$(ODIR)/%.o_contact : $(ODIR)/%.cbinned 
	@echo /////////////////////////////////////////////////////////////////////////////////////////
	@echo Observed matrix, $*
	$(CURDIR)/lscripts/observed_cbin_counts.pl $(ODIR)/$* $(MAX_COUNT) 0 0 $@

$(ODIR)/%.e_contact: $(ODIR)/%.o_contact
	@echo /////////////////////////////////////////////////////////////////////////////////////////
	@echo Expected matrix, $*
	$(CURDIR)/lscripts/cis_expected_cbins_count.pl $(ODIR)/$* $(OUT_BINSIZE_EXP_MAX_DIST) e F

contact_map: $(ODIR)/$(DATASET)_$(BINSIZE).e_contact

#####################################################################################################
# Quality control figures and metrics
#####################################################################################################

# List of chromosomes
ALL_CHROMS?=$(shell cut -f 1 $(CHROM_SIZES) | tr '\n' ' ' | sed 's/ $$//g' )

qc:
	$(CURDIR)/R/call.r $(CURDIR) $(CURDIR)/R/scell_qc.r plot.qc.figures datasets=$(DATASET) odir=$(ODIR) fdir=$(FIGS_DIR) haploid=$(HAPLOID) chroms="$(ALL_CHROMS)"
	$(CURDIR)/lscripts/extract_stats.pl $(DATASET) $(ODIR)/qc $(ODIR)/$(DATASET).mat $(ODIR)/$(DATASET).stats $(ODIR)/qc/hpf/hpf_auto_$(DATASET).txt $(ODIR)/qc/hpf/hpf_X_$(DATASET).txt $(ODIR)/qc/hpf/hpf_obs_p_vals_$(DATASET).txt $(ODIR)/qc/cis_ratio/$(DATASET)_cis_vs_trans_log_enrich.txt


###############################################
#
# Ploting contact maps
#
###############################################

# datasets to plot (each dataset in separate plot)
CMAP_DATASETS?=$(DATASET)

# Chromosomes to plot in contact map (x and y axis)
CMAP_CHROMS_X?=$(ALL_CHROMS)
CMAP_CHROMS_Y?=$(CMAP_CHROMS_X)

# if ploting a single pair of chromosomes - can focus on ccoordinates to plot (from to, NULL to plot the full length)
CMAP_COORD_X?=NULL
CMAP_COORD_Y?=NULL

# output file name prefix
CMAP_OFN_PREFIX?="genome_wide"

# colormap colors
CMAP_COLORS?=white darkred purple black

# colormap values (should be equal number to colormap colors)
CMAP_BREAKS?=0 4 8 20

# binsizes to plot (smaller bins over larger ones)
CMAP_BINSIZES?=10000000

# plot size (inches)
CMAP_SIZE?=9

# plot only bins with more than min observed
CMAP_MIN_OBSERVED?=0

# rotate cmap (if ploting single chromosome, T/F)
CMAP_ROTATE?=F

# maximum distance between contacts to plot (cis)
CMAP_MAX_DIST?=NULL

plot_cmap:
	mkdir -p $(FIGS_DIR)/cmaps
	$(CURDIR)/R/call.r $(CURDIR) $(CURDIR)/R/cmap.r cmap.general \
		datasets=$(CMAP_DATASETS)			odir=$(ODIR) 		type='o' \
		fdir=$(FIGS_DIR)/cmaps/ 			title="$(CMAP_OFN_PREFIX)" \
		chrs.limit="$(CMAP_CHROMS_X)"		coord.limit="$(CMAP_COORD_X)" \
		chrs.limit.other="$(CMAP_CHROMS_Y)"	coord.limit.other="$(CMAP_COORD_Y)" \
		binsizes="$(CMAP_BINSIZES)" 		main.size.x=$(CMAP_SIZE) \
		min.observed=$(CMAP_MIN_OBSERVED) 	chrs.len.ifn=$(CHROM_SIZES) \
		colors="$(CMAP_COLORS)" 			breaks="$(CMAP_BREAKS)" \
		diagonal=$(CMAP_ROTATE) 			max.dist=$(CMAP_MAX_DIST)

plot_cis_cmap:
	make plot_cmap CMAP_OFN_PREFIX="cis_map"	CMAP_CHROMS_X=11 CMAP_CHROMS_Y=11 \
		CMAP_BINSIZES="250000 500000" CMAPSIZE=5 CMAP_MIN_OBSERVED=0

#################################################
.DELETE_ON_ERROR:
.SECONDARY:



