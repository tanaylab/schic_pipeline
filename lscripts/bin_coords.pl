#!/usr/bin/env perl

use strict;
use POSIX;
use warnings FATAL => qw(all);
use Data::Dumper;
use List::Util qw(first);

if ($#ARGV == -1) {
	print STDERR "usage: $0 <input file> <output file> <output bin index file> <T/F, T: fixed bin sizes (default), F: equal #fends per bin> <binsize/fends_per_bin> [ <max bin width> ] \n";
	exit 1;
}

my $ifn = $ARGV[0];
my $ofn = $ARGV[1];
my $ofn_bins = $ARGV[2];
my $fixed_binsize = $ARGV[3] eq "T";
my $binsize = $ARGV[4];
my $fends_per_bin = $ARGV[4];
my $max_bin_width = $#ARGV == 5 ? $ARGV[5] : -1;

print STDERR "Input: $ifn\nOut: $ofn\nOut bins: $ofn_bins\nFixed size: $fixed_binsize\nbin size/fends per bin: $binsize\nMax width: $max_bin_width\n\n";

# hash table with all fends
my %fends;
my $index = 1;
my %bins;

##########################################################################################
# traverse fends file
##########################################################################################

open(IN, $ifn) || die;
print STDERR "Reading input file $ifn into hash...\n";
my $header = <IN>;
chomp($header);
my %h = parse_header($header);

my $curr_chr = "";
my @c_arr;
my @f_arr;
my %c_fends;
while (my $line = <IN>) {
	chomp $line;
	my @f = split("\t", $line);

	my $fend = $f[$h{fend}];
	my $chr = $f[$h{chr}];
	my $coord = $f[$h{coord}];

	if ($fixed_binsize) {
		# get bin
		my $bcoord = int($coord / $binsize) * $binsize;

		# add to hash
		$fends{$chr} = {} if !defined($fends{$chr});
		if (!defined($fends{$chr}->{$bcoord})) {
			$fends{$chr}->{$bcoord} = {};
			$fends{$chr}->{$bcoord}->{index} = $index;
			$fends{$chr}->{$bcoord}->{lines} = {};
			$bins{$index} = {};
			$bins{$index}->{chr} = $chr;
			$bins{$index}->{from} = $bcoord;
			$bins{$index}->{to} = $bcoord + $binsize;
			$bins{$index}->{count} = 0;
			$index++;
		}
		# update bin counter
		$bins{$fends{$chr}->{$bcoord}->{index}}->{count}++;

		# add line
		$fends{$chr}->{$bcoord}->{lines}->{$fend} = $line;
	}
	else {
		if ($chr eq $curr_chr) {
			# add fend to chrom sorted fends array
			my $last_coord = pop (@c_arr);
			my $last_fend = pop (@f_arr);

			push (@c_arr, $coord < $last_coord ? ($coord, $last_coord) : ($last_coord, $coord));
			push (@f_arr, $coord < $last_coord ? ($fend, $last_fend) : ($last_fend, $fend));
		}
		else {
			#print STDERR "Processing chr $curr_chr\n";
			if ($#c_arr > 0) {
				# partition chrom fends array to equal size bins
				my $curr_bin_start_idx = 0;
				for (my $i = 0; $i <= $#c_arr; $i++) {
					if ($i - $curr_bin_start_idx == $fends_per_bin or 
						$c_arr[$i] - $c_arr[$curr_bin_start_idx] > $max_bin_width or
						$i == $#c_arr) {
						# terminate bin 
						my $prev_coord = $c_arr[$curr_bin_start_idx - ($curr_bin_start_idx > 0 ? 1 : 0)] ; 
						my $next_coord = $c_arr[$i - 1 + ($i < $#c_arr ? 1 : 0)]; 
						my $bcoord     = $c_arr[$curr_bin_start_idx];
						my $ecoord     = $c_arr[$i - 1];
						
						if (($next_coord + $ecoord - $bcoord - $prev_coord)/2 <= $max_bin_width) {
							$bcoord -= ceil(($bcoord - $prev_coord)/2);
							$ecoord += floor(($next_coord - $ecoord)/2);
						}

						$bins{$index} = {};
						$bins{$index}->{chr} = $curr_chr;
						$bins{$index}->{from} = $bcoord;
						$bins{$index}->{to} = $ecoord;
						$bins{$index}->{count} = $i - $curr_bin_start_idx;

						$fends{$curr_chr}->{$bcoord} = {};
						$fends{$curr_chr}->{$bcoord}->{index} = $index;
						$fends{$curr_chr}->{$bcoord}->{lines} = {};
						for (my $j = $curr_bin_start_idx; $j < $i; $j++) {
							$fends{$curr_chr}->{$bcoord}->{lines}->{$f_arr[$j]} = $c_fends{$f_arr[$j]};
						}
						$index++;	
						$curr_bin_start_idx = $i;
					}
				}
			}
			
			# Initialize new chr arrays
			undef(@c_arr);
			undef(@f_arr);
			undef(%c_fends);
			push (@c_arr, $coord);
			push (@f_arr, $fend);
		}
		$curr_chr = $chr;
		$c_fends{$fend} = $line;

	}
};
close(IN);

open(OUT, ">", $ofn) || die;
print OUT $header, "\tcoord_bin\n";
print STDERR "generating $ofn\n";

foreach my $chr (sort keys %fends) {
	foreach my $coord (sort {$a <=> $b} keys %{$fends{$chr}}) {
		foreach my $fend (sort {$a <=> $b} keys %{$fends{$chr}->{$coord}->{lines}}) {
			print OUT $fends{$chr}->{$coord}->{lines}->{$fend}, "\t", $fends{$chr}->{$coord}->{index}, "\n";
		}
	}
}
close(OUT);


open(OUT, ">", $ofn_bins) || die;
print OUT "cbin\tchr\tfrom.coord\tto.coord\tcount\n";
print STDERR "generating $ofn_bins\n";
foreach my $bin (sort {$a <=> $b} keys %bins) {
	print OUT $bin, "\t", $bins{$bin}->{chr}, "\t", 
	          $bins{$bin}->{from}, "\t", $bins{$bin}->{to}, "\t", $bins{$bin}->{count}, "\n";
}
close(OUT);

##########################################################################################

######################################################################################################
# Subroutines
######################################################################################################

sub parse_header
{
	my ($header) = @_;
	chomp($header);
	my @f = split("\t", $header);
	my %result;
	for (my $i = 0; $i <= $#f; $i++) {
		$result{$f[$i]} = $i;
	}
	return %result;
}

sub min
{
	my ($a, $b) = @_;

	return $a < $b ? $a : $b;
}
