#!/usr/bin/env perl

use strict;
use warnings FATAL => qw(all);

if ($#ARGV == -1) {
	print STDERR "usage: $0 <dataset> <odir> <paired fends (.mat)> <paired fends stats> <autosomes hits per fend file> <chrX hits per fend file> <obs vs exp hpf fn> <cis trans fn>\n";
	exit 1;
}

my $dataset = $ARGV[0];
my $odir = $ARGV[1];
my $mat_fn = $ARGV[2];
my $stats_fn = $ARGV[3];
my $auto_hpf_fn = $ARGV[4];
my $x_hpf_fn = $ARGV[5];
my $obs_vs_pred_hpf_fn = $ARGV[6];
my $cis_trans_ratio_fn = $ARGV[7];

##########################################################################################
# parse pair files
##########################################################################################

my $single_read_fends = 0;
my $n_normal_ligation = 0;
my $n_total_reads = 0;
my $n_unique_fends_pairs = 0;
my $n_total_fends_pairs = 0;
my $n_single_read_fend_pairs = 0;
my $auto_fends_3hits_p_val = 0;
my %auto_hits_per_fend;
my %x_hits_per_fend;



open(IN, $mat_fn) || die;
my $header = <IN>;
my %h = parse_header($header);
while (my $line = <IN>) 
{
    chomp $line;
    my @f = split("\t", $line);
    my $count = $f[$h{count}];
    if ($count == 1)
    {
		$single_read_fends++;
    }
}
close(IN);

open(STATS, $stats_fn) || die;
while (my $line = <STATS>)
{
    chomp $line;
    if ($line =~ /^total reads: (\d+)$/)
    {
	$n_total_reads = $1;
    }
    elsif ($line =~ /^normal ligation: (\d+) /)
    {
	$n_normal_ligation = $1;
    }
    elsif ($line =~ /^unique amplified fend pairs: (\d+)/)
    {
		$n_unique_fends_pairs = $1;
    }
    elsif ($line =~ /^total fend pairs: (\d+)/)
    {
		$n_total_fends_pairs = $1;
    }
    elsif ($line =~ /^non amplified pairs: (\d+)/)
    {
		$n_single_read_fend_pairs = $1;
    }
}
close(STATS);

open (CIS_TRANS, $cis_trans_ratio_fn) || die;
my $cis_trans_enrich_log_diff = <CIS_TRANS>;
chomp $cis_trans_enrich_log_diff;

%auto_hits_per_fend = get_hits_per_fend($auto_hpf_fn);
%x_hits_per_fend = get_hits_per_fend($x_hpf_fn);
my %hpf_vals = get_hpf_vals($obs_vs_pred_hpf_fn);

my $auto_hpf_ratios = $auto_hits_per_fend{2} > 0 ? ($auto_hits_per_fend{">3"} / $auto_hits_per_fend{2}) / ($auto_hits_per_fend{2} / $auto_hits_per_fend{1}) : 0;
my $x_hpf_ratios = $x_hits_per_fend{1} > 0 ? ($x_hits_per_fend{2} / $x_hits_per_fend{1}) / ($x_hits_per_fend{1} / $x_hits_per_fend{0}) : 0;
my $p_ratios = $x_hits_per_fend{2} > 0 ? ($x_hits_per_fend{2} / $x_hits_per_fend{1}) / ($auto_hits_per_fend{2} / $auto_hits_per_fend{1}) : 0;

#print STDERR "$dataset hpf auto:\t1=$auto_hits_per_fend{1}\t2=$auto_hits_per_fend{2}\t>3=".$auto_hits_per_fend{">3"}."\n";
#print STDERR "$dataset hpf x:\t1=$x_hits_per_fend{1}\t2=$x_hits_per_fend{2}\t>3=".$x_hits_per_fend{">3"}."\n";
system("mkdir -p $odir");
open(OUT, ">${odir}/".$dataset.".summary") || die;

my $l1 = "dataset".
	"\tUnique amplified fends pairs".
	"\tSingle read fend pairs".
	"\tPerc. of single read fend pairs".
	"\tPerc. of Normal ligation".
	"\tNormal ligation read pairs".
	"\tNon normal ligation read pairs".
	"\tCis-Trans enrich log diff";

my $l2 = "$dataset".
	"\t$n_unique_fends_pairs".
	"\t$n_single_read_fend_pairs".
	"\t".perc_str($n_single_read_fend_pairs, $n_total_fends_pairs).
	"\t".perc_str($n_normal_ligation, $n_total_reads).
	"\t$n_normal_ligation".
	"\t".($n_total_reads - $n_normal_ligation).
	"\t$cis_trans_enrich_log_diff";

# Autosomal hits per fends (== 3 hits)
foreach my $hits (keys %hpf_vals) 
{
	my $n_actual_chrs = $hpf_vals{$hits}{cells};
	my $n_sim_chrs = $n_actual_chrs * 2;
	
	$l1 = $l1 .
		"\tNum $n_actual_chrs chrs fends with $hits obs hits\t".
		"Predicted (binomial) num $n_actual_chrs chrs fends with $hits hits\t".
		"Pval (binomial) of num $n_actual_chrs chrs fends with $hits hits\t".
		"Simulated (with $n_sim_chrs chrs) num of $n_actual_chrs chrs fends with $hits hits\t".
		"Simulated Pval (with $n_sim_chrs chrs) of num of $n_actual_chrs chrs fends with $hits hits";

	$l2 = $l2 .
	"\t".$hpf_vals{$hits}{"obs"}.
	"\t".$hpf_vals{$hits}{"exp.binom"}.
	"\t".$hpf_vals{$hits}{"p.val.binom"}.
	"\t".$hpf_vals{$hits}{"exp.few.cells"}.
	"\t".$hpf_vals{$hits}{"p.val.few.cells"};
}

print OUT "$l1\n$l2\n";
close(OUT);

######################################################################################################
# Subroutines
######################################################################################################

sub parse_header
{
	my ($header) = @_;
	chomp($header);
	my @f = split("\t", $header);
	my %result;
	for (my $i = 0; $i <= $#f; $i++) {
		$result{$f[$i]} = $i;
	}
	return %result;
}

sub perc_str
{
	my ($n, $total) = @_;
	#print STDERR "$n\t$total\n";
	return ($total > 0 ? (int(1000 * $n / $total)/10) : 0);
}

sub get_hits_per_fend
{
    my ($hpf_fn) = @_;
    my %hits_per_fend = ('0' => 0, '1' => 0, '2' => 0, '>3' => 0);

    open(HPF, $hpf_fn) || die;
    $header = <HPF>;
    %h = parse_header($header);

    while (my $line = <HPF>) 
    {
	chomp $line;
	my @f = split("\t", $line);
	my $hits = $f[$h{"hits"}];
	my $count = $f[$h{count}];
	
	if ($hits >= 3)
	{
	    $hits_per_fend{">3"} += $count;
	}
	else
	{
	    $hits_per_fend{$hits} += $count;
	}
    }
    close(HPF);

    return %hits_per_fend;
}


sub get_hpf_vals
{
    my ($hpf_fn) = @_;
    my %vals = ();

    open(HPF, $hpf_fn) || die ("failed to open file: $hpf_fn\n");
    $header = <HPF>;
    %h = parse_header($header);

    while (my $line = <HPF>) 
    {
		chomp $line;
		my @f = split("\t", $line);
		my $hits = $f[$h{"hits"}];
		
		$vals{$hits} = {};
		$vals{$hits}{"cells"} = $f[$h{chrs}] / 2;
		$vals{$hits}{"obs"} = $f[$h{"obs.hits"}];
		$vals{$hits}{"exp.binom"} = $f[$h{"pred.binom.hits"}];
		$vals{$hits}{"p.val.binom"} = $f[$h{"p.val.binom"}];
		$vals{$hits}{"exp.few.cells"} = $f[$h{"sim.few.cells.hits"}];
		$vals{$hits}{"p.val.few.cells"} = $f[$h{"p.val.few.chrs"}];
    }
    close(HPF);

    return %vals;
}
