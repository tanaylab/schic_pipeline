#!/usr/bin/env perl

use strict;
use warnings FATAL => qw(all);

require "./lscripts/cbins_utils.pl";

if ($#ARGV == -1 or $#ARGV < 3) {
	print STDERR "\nusage: $0 <dataset prefix> <max distance> <output type e/n> <norm by cis decay T|F>\n\n\tInclude bin size in dataset prefix.\n\tComputes the expected value for a bin as the fraction of fend pairs in it from the total number of CIS fend pairs times the CIS observed pairs per chromosome (the later taken from the o_contact file).\n\tTo disable filtering by cis distance, set <max distance> to zero.\n\tIf <norm by cis decay> is T the sum of the expected value of all cbins pairs in distance d will be the same as the sum of the observed values in the corresponding cbins pairs.\n";
	exit 1;
}

my $prefix = $ARGV[0];
my $max_dist = $ARGV[1];
my $out_type = $ARGV[2];
my $norm_by_cis_dist = $ARGV[3] eq "T";

my $cbins_fn = "${prefix}.cbins";
my $contacts_fn = "${prefix}.o_contact";

if ((! -e $cbins_fn) or (! -e $contacts_fn)) { die "Files $cbins_fn / $contacts_fn not found\n"};

my $out_fn = "${prefix}.${out_type}_contact";
my $stats_fn = "${prefix}.stats";

my $debug = 0;
if ($debug)  
{ 
	open (STATS, ">$stats_fn") or die ($stats_fn);
}
#$debug or print STATS "obin1\tobin2\ttype\ttbin\n";

#############################################################################################
# read cbin file
#############################################################################################

our ($cbins_ref, $cbins_range_ref, $fends_per_chr_ref) = &read_cbins($cbins_fn);

our %cbins = %{ $cbins_ref };
our %cbins_range = %{ $cbins_range_ref };
our %fends_per_chr = %{ $fends_per_chr_ref };

my %obs_per_chr;
my %exp_per_chr;

#############################################################################################
# binned contacts file: only load cis bins
#############################################################################################

our %cbins_c;

open(IN, $contacts_fn) || die;
print STDERR "Processing file $contacts_fn ...\n";
my $header = <IN>;
my %h = parse_header($header);
my $c = 0;
my $c_tot = 0;
my $c_cis = 0;
while (my $line = <IN>) {
	chomp $line;
	#cbin1   cbin2   expected_count  observed_count

	$c++;
	$c_tot++;
	if ($c == 10000000) 
	{
		print STDERR "read $c_cis / $c_tot bins pairs (cis/total)\n";
		$c = 0;
	}

	my @f = split("\t", $line);
	my $cbin1 = $f[$h{coord_bin1}];
	my $cbin2 = $f[$h{coord_bin2}];
	my $ocount = $f[$h{unique_observed_count}];

	my $chr1 = $cbins{$cbin1}->{chr};
	my $chr2 = $cbins{$cbin2}->{chr};

	if ($chr1 eq $chr2 and $cbin1 <= $cbin2)
	{
		$c_cis++;

		if (! defined ($cbins_c{$chr1}))
		{
			$cbins_c{$chr1} = [];

			$obs_per_chr{$chr1} = {};
			$exp_per_chr{$chr1} = {};
		}
		if (! defined ($cbins_c{$chr1}[$cbin1]))
		{
			$cbins_c{$chr1}[$cbin1] = [];
		}
		

		$cbins_c{$chr1}[$cbin1][$cbin2] = {};

		$cbins_c{$chr1}[$cbin1][$cbin2]->{obs} = $ocount;
		$cbins_c{$chr1}[$cbin1][$cbin2]->{exp} = 0; # will be updated later for all cbins pairs, not only where we have observed values
		
		if (defined ($obs_per_chr{$chr1}->{$cbin2 - $cbin1}))
		{
			$obs_per_chr{$chr1}->{$cbin2 - $cbin1} += $ocount;
		}
		else
		{
			$obs_per_chr{$chr1}->{$cbin2 - $cbin1} = $ocount;
		}

	}
}
close(IN);


##################################################################
# Calculate expected matrix 
##################################################################
print STDERR "Calculating expected values...\n";

foreach my $chr (sort keys %cbins_c)
{
	#print STDERR "chr $chr...\n";
	for (my $d = 0; $d <= ($cbins_range{$chr}->{to} - $cbins_range{$chr}->{from}); $d++)
	{
		$exp_per_chr{$chr}->{$d} = 0;
		
		for (my $i = $cbins_range{$chr}->{from}; $i <= $cbins_range{$chr}->{to} - $d; $i++)
		{
			if ($d == 0) 
			{
				$cbins_c{$chr}[$i][$i+$d]->{exp} = ($cbins{$i}->{count} * ($cbins{$i}->{count} + 1))/2; 
			}
			else
			{
				$cbins_c{$chr}[$i][$i+$d]->{exp} = $cbins{$i}->{count} * $cbins{$i+$d}->{count}; 
			}
			
			$exp_per_chr{$chr}->{$d} += $cbins_c{$chr}[$i][$i+$d]->{exp};
		}
		
		if ($norm_by_cis_dist)
		{
			for (my $i = $cbins_range{$chr}->{from}; $i <= $cbins_range{$chr}->{to} - $d; $i++)
			{
				if (defined ($cbins_c{$chr}[$i][$i+$d]->{exp}) )
				{
					$cbins_c{$chr}[$i][$i+$d]->{exp} *= (defined($obs_per_chr{$chr}->{$d}) ? $obs_per_chr{$chr}->{$d} : 1) / $exp_per_chr{$chr}->{$d}
				}
			}
		}
	}
}

##################################################################
# Writing output file
##################################################################
print STDERR "Writing output file: $out_fn\n";

open(OUT, ">", $out_fn) || die "Failed to open $out_fn\n";
if ($out_type eq "e")
{
	print OUT "coord_bin1\tcoord_bin2\texpected_count\n";
}
elsif ($out_type eq "n")
{
	print OUT "cbin1\tcbin2\texpected_count\tobserved_count\n";
}
else
{
	die "Unknown output type ($out_type). Expecting n or e\n";
}

foreach my $chr (sort keys %cbins_c)
{
	my $p_cis;
	if ($norm_by_cis_dist)
	{
		$p_cis = 1;
	}
	else
	{
		my $sum_obs = 0;
		foreach my $v (values %{ $obs_per_chr{$chr} }) 
		{
			$sum_obs += $v;
		}

		my $sum_exp = 0;
		foreach my $v (values %{ $exp_per_chr{$chr} }) 
		{
			$sum_exp += $v;
		}

				
		$p_cis = $sum_obs / $sum_exp;
	}

	my $c2_init = $cbins_range{$chr}->{from};
	for (my $cbin1 = $cbins_range{$chr}->{from}; $cbin1 <= $cbins_range{$chr}->{to}; $cbin1++)
	{
		while ($max_dist > 0 and (abs($cbins{$c2_init}->{from} - $cbins{$cbin1}->{from}) > $max_dist))
		{
			$c2_init++;
		}
		for (my $cbin2 = $c2_init; $cbin2 <= $cbins_range{$chr}->{to}; $cbin2++)
		{
			my $exp_f = 1;

			my $c1 = $cbin1 < $cbin2 ? $cbin1 : $cbin2;
			my $c2 = $cbin1 < $cbin2 ? $cbin2 : $cbin1;

			if ($max_dist > 0 and (abs($cbins{$c2}->{from} - $cbins{$c1}->{from}) > $max_dist))
			{
				last;
			}

			my $obs =  defined($cbins_c{$chr}[$c1][$c2]->{obs}) ? $cbins_c{$chr}[$c1][$c2]->{obs} : 0;
			my $exp = $cbins_c{$chr}[$c1][$c2]->{exp}  * $p_cis;

			if ($debug) 
			{
				print STATS "$chr\t$c1\t$c2\t$obs\t".($cbins_c{$chr}[$c1][$c2]->{exp})."\t$exp_f\t".($cbins_c{$chr}[$c1][$c2]->{exp}  * $exp_f)."\n";
			}

			print OUT "$cbin1\t$cbin2\t$exp". ($out_type eq "n" ? "\t$obs" : "")."\n";

		}
	}
}

close(OUT);
if ($debug) 
{
	close(STATS);
}



