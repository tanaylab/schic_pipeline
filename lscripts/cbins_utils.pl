#!/usr/bin/env perl

use strict;
use warnings FATAL => qw(all);


#############################################################################################
# read cbin file
#############################################################################################

sub read_cbins()
{
	my ($cbins_fn) = $_[0];

	my %cbins;
	my %cbins_range;
	my %fends_per_chr; 

	open(IN, $cbins_fn) || die($cbins_fn);
	print STDERR "Reading file $cbins_fn into hash...\n";
	my $header = <IN>;
	my %h = parse_header($header);
	my $max_cbin = 0;
	while (my $line = <IN>) {
		chomp $line;
		# cbin    chr     from.coord      to.coord
		my @f = split("\t", $line);
		my $cbin = $f[$h{cbin}];
		my $chr = $f[$h{chr}];
		my $count = $f[$h{"count"}];
		$cbins{$cbin} = {};
		$cbins{$cbin}->{chr} = $chr;
		$cbins{$cbin}->{from} = $f[$h{"from.coord"}];
		$cbins{$cbin}->{to} = $f[$h{"to.coord"}];
		$cbins{$cbin}->{count} = $count;
		
		if (!defined ($cbins_range{$chr}) ) 
		{
			$cbins_range{$chr} = {};
			$cbins_range{$chr}->{from} = $cbin;
			$cbins_range{$chr}->{to} = $cbin;
		}
		
		$cbins_range{$chr}->{from} = $cbins_range{$chr}->{from} > $cbin ? $cbin : $cbins_range{$chr}->{from};
		$cbins_range{$chr}->{to}   = $cbins_range{$chr}->{to}   < $cbin ? $cbin : $cbins_range{$chr}->{to};
		
		$fends_per_chr{$chr} = (defined $fends_per_chr{$chr} ? $fends_per_chr{$chr} : 0) + $count; 
	}
	close(IN);

#	foreach my $chr (sort keys %cbins_range) 
#	{
#		print STDERR "$chr\t".$cbins_range{$chr}->{from}."\t".$cbins_range{$chr}->{to}."\n";
#	}

	return (\%cbins, \%cbins_range, \%fends_per_chr);
}

#############################################################################################
# binned contacts file: only load cis bins (return hash of 2d array of hash: {chr}[cbin1][cbin2]->{obs/exp}
#############################################################################################

sub read_cis_n_contact()
{
	my ($contacts_fn, $cbins_ref) = @_;

	my %cbins_c;
	my %cbins = %{ $cbins_ref};

	open(IN, $contacts_fn) || die;
	print STDERR "Processing file $contacts_fn ...\n";
	my $header = <IN>;
	my %h = parse_header($header);
	while (my $line = <IN>) {
		chomp $line;
		#cbin1   cbin2   expected_count  observed_count

		my @f = split("\t", $line);
		my $cbin1 = $f[$h{cbin1}];
		my $cbin2 = $f[$h{cbin2}];
		my $ocount = $f[$h{observed_count}];
		my $ecount = $f[$h{expected_count}];;
		my $chr1 = $cbins{$cbin1}->{chr};
		my $chr2 = $cbins{$cbin2}->{chr};

		if ($chr1 eq $chr2 and $cbin1 <= $cbin2)
		{
			if (! defined ($cbins_c{$chr1}))
			{
				$cbins_c{$chr1} = [];
			}
			if (! defined ($cbins_c{$chr1}[$cbin1]))
			{
				$cbins_c{$chr1}[$cbin1] = [];
			}
			$cbins_c{$chr1}[$cbin1][$cbin2] = {};

			$cbins_c{$chr1}[$cbin1][$cbin2]->{obs} = $ocount;
			$cbins_c{$chr1}[$cbin1][$cbin2]->{exp} = $ecount;
		}
	}
	close(IN);

	return \%cbins_c;
}

#############################################################################################
# binned contacts file: only load cis bins (return hash with these levels: {chr}->{from1}->{from2}->{obs/exp}
#############################################################################################

sub read_cis_n_contact_by_coord()
{
	my ($contacts_fn, $cbins_ref) = @_;

	my %cbins_c;
	my %cbins = %{ $cbins_ref};

	open(IN, $contacts_fn) || die;
	print STDERR "Processing file $contacts_fn ...\n";
	my $header = <IN>;
	my %h = parse_header($header);
	while (my $line = <IN>) {
		chomp $line;
		#cbin1   cbin2   expected_count  observed_count

		my @f = split("\t", $line);
		my $cbin1 = $f[$h{cbin1}];
		my $cbin2 = $f[$h{cbin2}];
		my $ocount = $f[$h{observed_count}];
		my $ecount = $f[$h{expected_count}];;
		my $chr1 = $cbins{$cbin1}->{chr};
		my $chr2 = $cbins{$cbin2}->{chr};
		my $f1 = $cbins{$cbin1}->{from};
		my $f2 = $cbins{$cbin2}->{from};

		if ($chr1 eq $chr2 and $cbin1 <= $cbin2)
		{
			if (! defined ($cbins_c{$chr1}))
			{
				$cbins_c{$chr1} = {};
			}
			if (! defined ($cbins_c{$chr1}->{$f1}))
			{
				$cbins_c{$chr1}->{$f1} = {};
			}
			$cbins_c{$chr1}->{$f1}->{$f2} = {};

			$cbins_c{$chr1}->{$f1}->{$f2}->{obs} = $ocount;
			$cbins_c{$chr1}->{$f1}->{$f2}->{exp} = $ecount;
		}
	}
	close(IN);

	return \%cbins_c;
}

######################################################################################################
# Subroutines
######################################################################################################
sub parse_header
{
	my ($header) = @_;
	chomp($header);
	my @f = split("\t", $header);
	my %result;
	for (my $i = 0; $i <= $#f; $i++) {
		$result{$f[$i]} = $i;
	}
	return %result;
}

1
