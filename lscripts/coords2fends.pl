#!/usr/bin/env perl

use strict;
use warnings FATAL => qw(all);
use Data::Dumper;

if ($#ARGV == -1) {
    print STDERR "usage: $0 <fends> <paired reads raw> <read length> <output stats> <output mat> <dataset> <only normal pairs T|F> <remove one time reads T|F> [ <non unique fends file> ]\n";
    exit 1;
}

my $in_fends_fn = $ARGV[0];
my $raw_fn = $ARGV[1];
my $read_len = $ARGV[2];
my $stats_fn = $ARGV[3];
my $mat_fn = $ARGV[4];
my $dataset = $ARGV[5];
my $only_normal = $ARGV[6] eq "T";
my $discard_single = $ARGV[7] eq "T";
my $non_unique_fends_fn = $#ARGV >= 8 ? $ARGV[8] : "";
my $pairs_fn = $mat_fn.".dist";

our %fends;
our %nu_fends;
our %fend_matrix;
our %fends_covered;
our %dangling_fends;

# to quickly get from approximate coord to fend
our %coord2index;

# in debug mode output info on each read-pair into $pairs_fn
my $debug = 0;

##########################################################################################
# read fends file
##########################################################################################

open(IN, $in_fends_fn) || die $in_fends_fn;
print STDERR "Reading input file $in_fends_fn into hash...\n";
my $header = <IN>;
my %h = parse_header($header);
while (my $line = <IN>) {
    chomp $line;
    my @f = split("\t", $line);
    my $fend = $f[$h{fend}];
    my $chr = $f[$h{chr}];
    my $coord = $f[$h{coord}];
    my $strand = $f[$h{strand}];
    my $frag_len = $f[$h{frag_len}];
    my $fragend_len = $f[$h{fragend_len}];
    my $next_fragends_lens = $f[$h{next_fragend_len}] if $debug;

    $fragend_len = $frag_len if ($fragend_len > $frag_len);

    !defined($fends{$fend}) or die "non-unique fend";
    $fends{$fend} = {};
    $fends{$fend}->{fend} = $fend;
    $fends{$fend}->{frag} = $f[$h{frag}];
    $fends{$fend}->{chr} = $chr;
    $fends{$fend}->{coord} = $coord;
	
    $fends{$fend}->{next_cutter_dists} = $next_fragends_lens if $debug;

    # not needed
    #$fends{$fend}->{strand} = $strand;
    #$fends{$fend}->{frag_len} = $frag_len;
    #$fends{$fend}->{fragend_len} = $fragend_len;

    $fends{$fend}->{cutter_coord} = $strand eq "+" ? $coord + $fragend_len : $coord - $fragend_len;

#    my @next_cutter_dists = ();
#    for my $s (split(/,/, $next_fragends_lens)) {
#	push (@next_cutter_dists, $strand eq "+" ? $coord + $s : $coord - $s);
#    }
#    $fends{$fend}->{next_cutter_dists} = [ split(/,/, $next_fragends_lens) ] ;
	
    if (!defined($coord2index{$chr}))
    {
		$coord2index{$chr} = {};
		$coord2index{$chr}->{coords} = {};
    }
    $coord2index{$chr}->{coords}->{$coord} = $fend;
}
close(IN);

##########################################################################################
# read non unique fends file (if supplied)
##########################################################################################
if ($non_unique_fends_fn ne "") {
	open(IN, $non_unique_fends_fn) || die $non_unique_fends_fn;
	print STDERR "Reading non unique fends file $non_unique_fends_fn into hash...\n";
	my $header = <IN>;
	my %h = parse_header($header);
	while (my $line = <IN>) {
		chomp $line;
		my @f = split("\t", $line);
		my $fend = $f[$h{fend}];
		!defined($nu_fends{$fend}) or die "duplicate non-unique fend";
		$nu_fends{$fend} = 1;
	}
	close(IN);
}

#########################################################################################
# compute sorted coords per chrom
##########################################################################################

for my $chr (%coord2index)
{
    my @sorted = sort {$a <=> $b} keys %{$coord2index{$chr}->{coords}};
    $coord2index{$chr}->{sorted_coords} = \@sorted;
}

##########################################################################################
# parse pair files
##########################################################################################


#my $appr_lines = apprx_lines($raw_fn);
#print STDERR "traversing file $raw_fn, with about ".int($appr_lines/1000000)."M lines\n";
print STDERR "traversing file $raw_fn\n";

my $read_count = 0;

our %read_stats;
$read_stats{fend_not_found} = 0;
$read_stats{no_ligation} = 0;
$read_stats{no_ligation_middle} = 0;
$read_stats{self_ligation} = 0;
$read_stats{self_ligation_strange} = 0;
$read_stats{re_ligation} = 0;
$read_stats{normal_ligation_missed_re2} = 0;
$read_stats{normal_ligation} = 0;

if ($debug) 
{
	open(PAIRS, ">", $pairs_fn) || die;
	print PAIRS "type\tfend1\tchr1\tcoord1\tfirst_dist1\tfirst_re2_dist1\thit_re2_1\tnearest_re2_dist1\tfend2\tchr2\tcoord2\tfirst_dist2\tfirst_re2_dist2\thit_re2_2\tnearest_re2_dist2\n";
}

open(IN, $raw_fn) || die;
$header = <IN>;
%h = parse_header($header);
while (my $line = <IN>) 
{
    my $valid_pair = "";
    $read_count++;
    print STDERR "line: $read_count\n" if ($read_count % 1000000 == 0);

    chomp $line;
    my @f = split("\t", $line);
    my $chr1 = $f[$h{chr1}];
    my $coord1 = $f[$h{coord1}];
    my $strand1 = $f[$h{strand1}];
    my $chr2 = $f[$h{chr2}];
    my $coord2 = $f[$h{coord2}];
    my $strand2 = $f[$h{strand2}];

    $coord1 += $read_len-4 if ($strand1 eq "-");
    $coord2 += $read_len-4 if ($strand2 eq "-");
    $coord1 -= 3 if ($strand1 eq "+");
    $coord2 -= 3 if ($strand2 eq "+");

    my ($fend1, $fend_coord1, $index1) = find_closest_fend($chr1, $coord1, $strand1);
    my ($fend2, $fend_coord2, $index2) = find_closest_fend($chr2, $coord2, $strand2);

#    if ($fend1 == 1054713 or $fend2 == 1054713)
#    {
#	print STDERR "Debug\n";
#    }
    
    if ($fend1 == -1 or $fend2 == -1)
    {
		$read_stats{fend_not_found}++;
		next;
    }

    my $frag1 = $fends{$fend1}->{frag};
    my $frag2 = $fends{$fend2}->{frag};

    # true iff reads are facing each other
    my $face_towards = (($coord1 < $coord2) && ($strand1 eq "+") && ($strand2 eq "-")) ||
		(($coord2 < $coord1) && ($strand1 eq "-") && ($strand2 eq "+"));

    my $face_otherway = (($coord1 < $coord2) && ($strand1 eq "-") && ($strand2 eq "+")) ||
		(($coord2 < $coord1) && ($strand1 eq "+") && ($strand2 eq "-"));

    my $not_ligated = ($chr1 eq $chr2) && $face_towards &&
		(between($coord1, $fend_coord1, $coord2) ||
		 between($coord2, $fend_coord2, $coord1));
    if ($not_ligated)
    {
		#(($frag1 == $frag2) and (abs($index1-$index2) <= 1)) or die;
		if ((abs($frag1 - $frag2) > 1) or (abs($index1-$index2) > 1)) {
			die "frag1=$frag1\tfrag2=$frag2\tfend1=$fend1\tfend2=$fend2\tcoord1=$coord1\tcoord2=$coord2\n";
		}
		if (abs($coord2 - $fend_coord1) < 50) {
			$read_stats{no_ligation}++;
			$dangling_fends{$fend1} = 1;
		} elsif (abs($coord1 - $fend_coord2) < 50) {
			$read_stats{no_ligation}++;
			$dangling_fends{$fend2} = 1;
		} else {
			$read_stats{no_ligation_middle}++;
		}
		next;
    }

    # 1st cutter dist
    my $first_dist1 = abs($fends{$fend1}->{coord} - $coord1);
    my $first_dist2 = abs($fends{$fend2}->{coord} - $coord2);

    # 2nd cutter dist
    my $dist1 = abs($fends{$fend1}->{cutter_coord} - $coord1);
    my $dist2 = abs($fends{$fend2}->{cutter_coord} - $coord2);

	my ($re2_hit_1, $re2_hit_dist1) = match_dist_to_re2_site($first_dist1, $fends{$fend1}->{next_cutter_dists}) if $debug;
	my ($re2_hit_2, $re2_hit_dist2) = match_dist_to_re2_site($first_dist2, $fends{$fend2}->{next_cutter_dists}) if $debug;

    my $pair_type;
    if (($chr1 eq $chr2) && ($frag1 eq $frag2)) {
		# two ends of one fragment got fused
		$read_stats{self_ligation}++;
		$pair_type = "Self ligation";
    } elsif (($chr1 eq $chr2) && (abs($frag1-$frag2) == 1) && (abs($index1 - $index2) == 1)) {
		# site got broken and got fused back
		$read_stats{re_ligation}++;
		$pair_type = "Re-ligation";
    } elsif (($dist1 + $dist2) > 10) {
		$read_stats{normal_ligation_missed_re2}++;
		$pair_type = "Normal ligation missed first RE2";
    } else {
		$valid_pair = 1;
		$read_stats{normal_ligation}++;
		$pair_type = "Normal ligation";
    }

	if ($debug) 
	{
		print PAIRS "$pair_type\t$fend1\t$chr1\t$coord1\t$first_dist1\t$dist1\t$re2_hit_1\t$re2_hit_dist1\t";
		print PAIRS             "$fend2\t$chr2\t$coord2\t$first_dist2\t$dist2\t$re2_hit_2\t$re2_hit_dist2\n";
	}

    if ($valid_pair or !$only_normal)
    {
		# track number of unique fends
		$fends_covered{$fend1} = 1;
		$fends_covered{$fend2} = 1;

		my $fend_small = ($fend1 <= $fend2) ? $fend1 : $fend2;
		my $fend_large = ($fend1 <= $fend2) ? $fend2 : $fend1;
		$fend_matrix{$fend_small} = {} if !defined($fend_matrix{$fend_small});
		$fend_matrix{$fend_small}->{$fend_large} = 0 if !defined($fend_matrix{$fend_small}->{$fend_large});
		$fend_matrix{$fend_small}->{$fend_large}++;
    }
}
close(IN);

close(PAIRS) if ($debug);


my %fend_stats;
$fend_stats{far_cis} = 0;
$fend_stats{same_fend} = 0;
$fend_stats{self_ligation} = 0;
$fend_stats{re_ligation} = 0;
$fend_stats{close_cis} = 0;
$fend_stats{trans} = 0;
$fend_stats{not_found} = 0;
$fend_stats{non_unique} = 0;
$fend_stats{one_time_fend_pair} = 0;


my $fend_count = 0;
open(OUT, ">", $mat_fn) || die;

#print OUT "fend1\tchr1\tcoord1\tfend2\tchr2\tcoord2\tcount\n";
print OUT "fend1\tfend2\tcount\n";

foreach my $fend1 (sort { $a <=> $b } keys %fend_matrix)
{
    foreach my $fend2 (sort { $a <=> $b } keys %{$fend_matrix{$fend1}})
    {
		$fend_count++;
		my $count = $fend_matrix{$fend1}->{$fend2};

		#print OUT $fend1, "\t" , $fends{$fend1}->{chr}, "\t", $fends{$fend1}->{coord}, "\t",
		#          $fend2, "\t" , $fends{$fend2}->{chr}, "\t", $fends{$fend2}->{coord}, "\t", $count, "\n";

		if ($non_unique_fends_fn ne "" and (defined($nu_fends{$fend1}) or defined($nu_fends{$fend2}))) {
			$fend_stats{non_unique}++;
			next;
		}

		
		if (!$discard_single or $count > 1) {
			print OUT $fend1, "\t" , $fend2, "\t" , $count, "\n";
		}

		# fend pair appeared once (not amplified)
		if ($count == 1) {
			$fend_stats{one_time_fend_pair}++;
			next;
		}

		# fend to itself (small chance in single cell)
		if ($fend1 == $fend2) {
			$fend_stats{same_fend}++;
			next;
		}

		# trans
		if ($fends{$fend1}->{chr} ne $fends{$fend2}->{chr}) {
			$fend_stats{trans}++;
			next;
		}

		# cis
		if ($fends{$fend1}->{frag} == $fends{$fend2}->{frag}) {
			$fend_stats{self_ligation}++;
		} elsif (abs($fends{$fend1}->{coord} - $fends{$fend2}->{coord}) <= 1) {
			$fend_stats{re_ligation}++;
		} elsif (abs($fends{$fend1}->{coord} - $fends{$fend2}->{coord}) < 10000) {
			$fend_stats{close_cis}++;
		} else {
			$fend_stats{far_cis}++;
		}
    }
}
close(OUT);

######################################################################################################
# Write fend_stats
######################################################################################################

open(OUT, ">", $stats_fn) || die;
print OUT "total reads: $read_count\n";
print OUT "fend not found (probably end of chr): ", perc_str($read_stats{fend_not_found}, $read_count), "\n";
print OUT "no ligation: ", perc_str($read_stats{no_ligation_middle}, $read_count), "\n";
print OUT "no ligation (RE1 <-> RE2): ", perc_str($read_stats{no_ligation}, $read_count), "\n";
print OUT "frag self ligation: ", perc_str($read_stats{self_ligation}, $read_count), "\n";
print OUT "frag self ligation (bad strands): ", perc_str($read_stats{self_ligation_strange}, $read_count), "\n";
print OUT "re ligation: ", perc_str($read_stats{re_ligation}, $read_count), "\n";
print OUT "normal ligation, missed RE2: ", perc_str($read_stats{normal_ligation_missed_re2}, $read_count), "\n";
print OUT "normal ligation: ", perc_str($read_stats{normal_ligation}, $read_count), "\n";
print OUT "--------------------------------\n";
print OUT "total fend pairs: $fend_count\n";
print OUT "pairs with non unique fend: " . perc_str($fend_stats{non_unique}, $fend_count) . "\n";
print OUT "non amplified pairs: " . perc_str($fend_stats{one_time_fend_pair}, $fend_count) . "\n";
my $unique_amp_pairs = $fend_count - $fend_stats{non_unique} - $fend_stats{one_time_fend_pair};
print OUT "unique amplified fend pairs: " . perc_str($unique_amp_pairs, $fend_count)."\n";
print OUT "-------------\n";
print OUT "fend to itself: ", perc_str($fend_stats{same_fend}, $unique_amp_pairs), "\n";
print OUT "frag self ligation: ", perc_str($fend_stats{self_ligation}, $unique_amp_pairs), "\n";
print OUT "re ligation: ", perc_str($fend_stats{re_ligation}, $unique_amp_pairs), "\n";
print OUT "close cis pairs (<10k): ", perc_str($fend_stats{close_cis}, $unique_amp_pairs), "\n";
print OUT "far cis pairs (>10k): ", perc_str($fend_stats{far_cis}, $unique_amp_pairs), "\n";
print OUT "trans pairs: ", perc_str($fend_stats{trans}, $unique_amp_pairs), "\n";
print OUT "-------------------------------------\n";
print OUT "covered fends: ", scalar keys %fends_covered, "\n";
print OUT "dangling fends: ", scalar keys %dangling_fends, "\n";
close(OUT);

# make reads table
my $read_table_fn = $mat_fn.".read_table";
open(READS, ">", $read_table_fn) || die;
print READS "type\t$dataset\n";
print READS "total\t$read_count\n";
print READS "fend not found\t", $read_stats{fend_not_found}, "\n";
print READS "no ligation\t ", $read_stats{no_ligation_middle}, "\n";
print READS "no ligation (RE1 <-> RE2)\t ", $read_stats{no_ligation}, "\n";
print READS "frag self ligation\t ", $read_stats{self_ligation}, "\n";
print READS "frag self ligation (bad strands)\t", $read_stats{self_ligation_strange}, "\n";
print READS "re ligation\t", $read_stats{re_ligation}, "\n";
print READS "normal ligation, missed RE2\t", $read_stats{normal_ligation_missed_re2}, "\n";
print READS "normal ligation\t", $read_stats{normal_ligation}, "\n";
close(READS);

# make fends table
my $fend_table_fn = $mat_fn.".fend_table";
open(FENDS, ">", $fend_table_fn) || die;
print FENDS "type\t$dataset\n";
print FENDS "total\t$fend_count\n";
print FENDS "fend to itself\t", $fend_stats{same_fend}, "\n";
print FENDS "frag self ligation\t", $fend_stats{self_ligation}, "\n";
print FENDS "pairs with non unique fend\t", $fend_stats{non_unique}, "\n";
print FENDS "non amplified pairs\t", $fend_stats{one_time_fend_pair}, "\n";
print FENDS "unique amplified fend pairs\t", $unique_amp_pairs, "\n";
print FENDS "re ligation\t", $fend_stats{re_ligation}, "\n";
print FENDS "close cis pairs (<10k)\t", $fend_stats{close_cis}, "\n";
print FENDS "far cis pairs (>10k)\t", $fend_stats{far_cis}, "\n";
print FENDS "trans pairs\t", $fend_stats{trans}, "\n";
close(FENDS);

######################################################################################################
# Subroutines
######################################################################################################

# check if C is between and A,B
sub between
{
    my ($A, $B, $C) = @_;
    $B = $B - $A;
    $C = $C - $A;
    return (($B>$C && $C>0) || ($B<$C && $C<0));
}

sub add_fend
{
    my ($chr, $bin, $fend, $strand) = @_;
    $coord2index{$chr} = {} if !defined($coord2index{$chr});
    $coord2index{$chr}->{$bin} = {} if !defined($coord2index{$chr}->{$bin});

    # mark bin if multiple fends map to it
    if (!defined($coord2index{$chr}->{$bin}->{$strand}))
    {	
		$coord2index{$chr}->{$bin}->{$strand} = $fend;
    }
    else 
    {
		$coord2index{$chr}->{$bin}->{$strand} = -1;
    }
}

sub apprx_lines
{
    my ($fn) = @_;
    my $tmp = "/tmp/".$$."_apprx_lines.tmp";
    system("head -n 100000 $fn > $tmp");
    my $size_head = -s $tmp;
    my $size_all = -s $fn;
    $size_head > 0 or die;
    return (int($size_all/$size_head*100000));
}

sub perc_str
{
    my ($n, $total) = @_;
    #print STDERR "$n\t$total\n";
    return ($n." (".($total > 0 ? (int(1000 * $n / $total)/10) : 0)."%)");
}

sub find_closest_fend
{
    my ($chr, $coord, $strand) = @_;
    return ((-1,-1,-1)) if (!defined($coord2index{$chr}));
    my $index = ($strand eq "+") ?
		binary_search($coord2index{$chr}->{sorted_coords}, $coord, 1) :
		binary_search($coord2index{$chr}->{sorted_coords}, $coord, 0);
    return ((-1,-1,-1)) if ($index == -1);
    my $fend_coord = $coord2index{$chr}->{sorted_coords}[$index];
    defined ($coord2index{$chr}->{coords}->{$fend_coord}) and defined($fend_coord) or die;
    return (($coord2index{$chr}->{coords}->{$fend_coord}, $fend_coord, $index));
}

sub match_dist_to_re2_site
{
    my ($dist, $re2_dists_str) = @_;

    my @dists = split (/,/, $re2_dists_str);
#    my $hit = 0;
#    while ($hit <= $#dists and $dists[$hit] < $dist) {
#	$hit++;
#    }
    my $hit = binary_search (\@dists, $dist, 1);
    my $re2_dist = $dists[$hit] - $dist;
    return ($hit, $re2_dist);
}

# returns first element above/below value in sorted array
sub binary_search 
{
    my $arr = shift;
    my $value = shift;
    my $above = shift; 

    my $left = 0;
    my $right = $#$arr;
    
    while ($left <= $right) {
		my $mid = ($right + $left) >> 1;
		my $c = $arr->[$mid] <=> $value;
		return $mid if ($c == 0);
		if ($c > 0) {
			$right = $mid - 1;
		} else {
			$left  = $mid + 1;
		}
    }
    $left = -1 if ($left > $#$arr);
    $right = -1 if ($right < 0);
    return ($above ? $left : $right);
}

sub parse_header
{
    my ($header) = @_;
    chomp($header);
    my @f = split("\t", $header);
    my %result;
    for (my $i = 0; $i <= $#f; $i++) {
		$result{$f[$i]} = $i;
    }
    return %result;
}



